import os, sys
import MySQLdb, happybase

MYSQL_CHARSET = "utf8mb4"



def get_db(storage_name, endpoint, port=None, user=None, passwd=None, dbname=None):
    storage_name = storage_name.lower()
    if storage_name == "mysql":
        return MySQLdb.connect(host=endpoint, port=port, user=user, passwd=passwd, db=dbname, charset=MYSQL_CHARSET)
    elif storage_name == "hbase":
        return happybase.Connection(endpoint)
    else:
        raise Exception("Unsupported storage_name")

def drop_table_mysql(db, tweettable, retweettable, retweet_postprocess_table, hashtagtable):
    cursor = db.cursor()

    cursor.execute("DROP TABLE IF EXISTS %s" %(tweettable))
    cursor.execute("DROP TABLE IF EXISTS %s" %(retweettable))
    cursor.execute("DROP TABLE IF EXISTS %s" %(retweet_postprocess_table))
    cursor.execute("DROP TABLE IF EXISTS %s" %(hashtagtable))
    
    db.commit()

def create_table_mysql(db, tweettable, retweettable, retweet_postprocess_table, hashtagtable):
    cursor = db.cursor()
    sql = "CREATE TABLE IF NOT EXISTS %s" %(tweettable) + \
                    "(`tid` BIGINT NOT NULL, content VARCHAR(1024) CHARACTER SET " + MYSQL_CHARSET + " COLLATE " + MYSQL_CHARSET + "_unicode_ci NOT NULL," + \
                    "score INT NOT NULL, time DATETIME NOT NULL, PRIMARY KEY (tid), user_id BIGINT NOT NULL)" + \
                    " CHARACTER SET %s COLLATE %s_unicode_ci" %(MYSQL_CHARSET, MYSQL_CHARSET)
    cursor.execute(sql)

    sql = "CREATE TABLE IF NOT EXISTS %s" %(retweettable) + \
            "(user_id BIGINT NOT NULL, retweeter_id BIGINT NOT NULL, PRIMARY KEY (user_id, retweeter_id))";
    cursor.execute(sql)

    sql = "CREATE TABLE IF NOT EXISTS %s" %(retweet_postprocess_table) + \
            "(user_id BIGINT NOT NULL, retweeter_ids LONGTEXT NOT NULL, PRIMARY KEY (user_id))";
    cursor.execute(sql)

    sql = "CREATE TABLE IF NOT EXISTS %s" %(hashtagtable) + \
            "(day VARCHAR(20) NOT NULL, location VARCHAR(512) CHARACTER SET " + MYSQL_CHARSET + " COLLATE " + MYSQL_CHARSET + "_unicode_ci NOT NULL," + \
            "hashtag VARCHAR(1024) CHARACTER SET " + MYSQL_CHARSET + " COLLATE " + MYSQL_CHARSET + "_unicode_ci NOT NULL, " + \
            "popularity INT NOT NULL, tweet_ids TEXT NOT NULL, indice INT NOT NULL)"
    cursor.execute(sql)

    db.commit()

def create_table_hbase(db, tweettable, retweettable, retweet_postprocess_table, hashtagtable):
    for tablename in (tweettable, retweettable, retweet_postprocess_table, hashtagtable):
        if tablename not in db.tables():
            db.create_table(tablename, {'phd': {}})

def drop_table_hbase(db, tweettable, retweettable, retweet_postprocess_table, hashtagtable):
    for tablename in (tweettable, retweettable, retweet_postprocess_table, hashtagtable):
        if tablename in db.tables():
            db.delete_table(tablename, True)


def main():
    dbtype = sys.argv[1]
    dbconnection_str = sys.argv[2]

    tweettable, retweettable, retweet_postprocess_table, hashtagtable = "tweet", "retweet", "retweet_post", "hashtag"

    if dbtype == "mysql":
        [endpoint, port, user, passwd, dbname] = dbconnection_str.split(",")
        db = get_db(dbtype, endpoint, int(port), user, passwd, dbname)
        drop_table_mysql(db, tweettable, retweettable, retweet_postprocess_table, hashtagtable)
        create_table_mysql(db, tweettable, retweettable, retweet_postprocess_table, hashtagtable)

    elif dbtype == "hbase":
        endpoint = dbconnection_str
        db = get_db(dbtype, endpoint)
        drop_table_hbase(db, tweettable, retweettable, retweet_postprocess_table, hashtagtable)
        create_table_hbase(db, tweettable, retweettable, retweet_postprocess_table, hashtagtable)


if __name__ == '__main__':
    main()