# coding=utf-8
import MySQLdb, sys, os

def clean_sqlstr(string):
    return string.replace("<PHDNEWLINE>", "\n").replace("\\", "\\\\").replace("\'", "\\\'")

MYSQL_CHARSET = "utf8mb4"
def main():
    dbtype = sys.argv[1]
    dbconnection_str = sys.argv[2]

    [endpoint, port, user, passwd, dbname] = dbconnection_str.split(",")

    mysqldb = MySQLdb.connect(host=endpoint, port=int(port), user=user, passwd=passwd, db=dbname, charset=MYSQL_CHARSET)
    cursor = mysqldb.cursor()

    fs = open("../testdata_output")

    for line in fs:
        if line[-1] == '\n': line = line[: -1]

        if line.startswith("tweet"):
            line = line[6: ]
            [tid, content, score, timestr, user_id, raw] = line.split("\t")
            print type(content)

            sql = "INSERT INTO tweet (tid, content, score, time, user_id, raw) values(%s, '%s', %s, '%s', %s, '%s')" \
                                %(tid, clean_sqlstr(content).decode("utf-8"), score, timestr, user_id, clean_sqlstr(raw))

            cursor.execute(sql)

    mysqldb.commit()

    fs.close()


if __name__ == '__main__':
    main()