package emr.postprocess;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class RetweetPostProcessor {
	
	public static void main(String[] args) throws SQLException, IOException{
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String user = args[2],
				password = args[3];
		String dbName = args[4];
		String outputFile = args[5];
		BufferedWriter outputWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
		
		String url = String.format("jdbc:mysql://%s:%d/%s", host, port, dbName);
		//String url = String.format("jdbc:mysql://%s:%d/%s?useUnicode=true", host, port, dbName);
        Connection connection = DriverManager.getConnection(url, user, password);
        connection.setAutoCommit(false);
        
        
        String retweetTable = "retweet";
        String retweetPostTable = "retweet_post";
        
        connection.createStatement().executeUpdate("delete from " + retweetPostTable);
        connection.commit();
        
        
        long userCount = 0;
        ResultSet resultSet = connection.createStatement().executeQuery("select count(distinct(user_id)) from " + retweetTable);
        while(resultSet.next()) 
        	userCount = resultSet.getLong(1);
        
        
        // Read by batch
        int batchNum = 10000;
        long userIdIdx = 0;
        
        while(userIdIdx < userCount){
        	String sqlSelectAllUser = "select distinct(user_id) from " + retweetTable + " limit " + userIdIdx + "," + batchNum;
            resultSet = connection.createStatement().executeQuery(sqlSelectAllUser);
        
            while(resultSet.next()){
            	long userId = resultSet.getLong(1);
            	
            	String sqlSelectRetweeter = "select t1.retweeter_id, t1.user_id, t2.user_id from (select user_id, retweeter_id from retweet where user_id=" 
            	+ userId 
            	+ ") as t1 left join retweet as t2 on t1.retweeter_id=t2.user_id and t1.user_id=t2.retweeter_id;";
            	
            	// System.out.println(sqlSelectRetweeter);
            	StringBuffer stringBuffer = new StringBuffer();
            	ResultSet pairResultSet = connection.createStatement().executeQuery(sqlSelectRetweeter);
            	int idx = 0;
            	while(pairResultSet.next()){
            		Long retweeterId = pairResultSet.getLong(1),
            			 originalUserId = pairResultSet.getLong(2),
            			 retweetedByOriginalUserId = pairResultSet.getLong(3);
            		assert originalUserId == userId;
            		
            		if(idx > 0)
            			stringBuffer.append("\\n");
            		
            		if(retweetedByOriginalUserId == 0){
            			stringBuffer.append(retweeterId);
            		}else{
            			assert retweetedByOriginalUserId == retweeterId;
            			stringBuffer.append(String.format("(%d)", retweeterId));
            		}
            		idx += 1;
            	}
            	outputWriter.write(userId + "\t'");
            	outputWriter.write(stringBuffer.toString());
            	outputWriter.write("'");
            	outputWriter.newLine();
            	outputWriter.flush();
            	// String insertSql = String.format("insert into %s (user_id, retweeter_ids) values(%d, '%s')", retweetPostTable, userId, stringBuffer.toString());
            	// connection.createStatement().executeUpdate(insertSql);
            	
            	userIdIdx += 1;
            	
            }
            
            // connection.commit();
            System.out.println(userIdIdx);
        }
        
        // Final commit
        // connection.commit();
        connection.close();
        outputWriter.close();
        
	}
}
