package emr;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

public class TwitterUserScoreReducer {
	
	public static void main(String[] args) throws IOException{
		Scanner reader = new Scanner(new InputStreamReader(System.in, Utils.ENCODING));
		reader.useDelimiter("\n");
		
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out, Utils.ENCODING));
		
		long score1 = 0,
			 score2 = 0,
			 score3 = 0;
		
		String lastUserScoreString = null;
		long lastUserId = -1;
		
		while(reader.hasNext()){
			String line = reader.next();
			if(line.isEmpty()){
				continue;
			}
			if(!line.startsWith("score_")){
				// Only reduce user scores
				
				if(line.startsWith("shutter")){
					// For shutter, directly output
					writer.write(line);
					writer.newLine();
					writer.flush();
				}else{
					// Other lines should be filtered by mapper, so there must be something wrong
					System.err.println(line);
				}
				
				continue;
			}
			String[] strings = line.split("\t");
			
			if(!strings[0].equals(lastUserScoreString)){
				// A new key -- a new user
				if(lastUserScoreString != null){
					// if we have last user, we flush the result
					assert lastUserId > 0;
					writeUserScoreSummary(writer, lastUserId, score1, score2, score3);
				}
			
				lastUserScoreString = strings[0];
				lastUserId = Long.parseLong(lastUserScoreString
						.substring(lastUserScoreString.indexOf('_') + 1));
				
				// reset scores
				score1 = 0;
				score2 = 0;
				score3 = 0;
			}
			
			int scoreType = Integer.parseInt(strings[1]);
			long score = Long.parseLong(strings[2]);
			if(scoreType == 1){
				score1 += score;
			}else if(scoreType == 2){
				score2 += score;
			}else if(scoreType == 3){
				score3 += score;
			}
			
		}
		
		if(lastUserScoreString != null){
			assert lastUserId > 0;
			writeUserScoreSummary(writer, lastUserId, score1, score2, score3);
		}
		writer.flush();
		writer.close();
		reader.close();
		
	}
	
	private static void writeUserScoreSummary(BufferedWriter writer, long userId, long score1, long score2, long score3) throws IOException{
		writer.write(String.format("score_%d\t%d\t%d\t%d\t%d", 
								   userId, score1, score2, score3, (score1 + score2 + score3)));
		writer.newLine();
		writer.flush();
	}
}
