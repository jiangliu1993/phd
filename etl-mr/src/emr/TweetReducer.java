package emr;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;


public class TweetReducer {
	
	/**
	 * Main entry for reducer
	 * Given input as mentioned in mapper
	 * We output the following lines
	 * 1. tweet tweet_id cleanedString score timeString userId rawString
	 * 2. retweet   <user_id>     retweeter_ids(id1,id2,...)
	 * 3. hashtag    <day>     <localtion>      <hashtag_name>     popularity      tweetid1,tweetid2    indice
	 * 4. shutter    <user_id>  <photo_count>
	 * 5. score_<user_id>     <score_id>    <score> This will be used as input of another map-reduce job
	 * @param args
	 * @throws EMRException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws EMRException, IOException{
		Scanner reader = new Scanner(new InputStreamReader(System.in, Utils.ENCODING));
		reader.useDelimiter("\n");
		
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out, Utils.ENCODING));
		
		// Match the tweet_tid
		String lastTweetInfoKey = null; 
		
		// Match the retweet_uid
		String lastRetweetKey = null; 
		long lastRetweetUserId = -1;
		List<Long> lastUserRetweeterIds = new Vector<Long>(); // user ids that retweeted the tweet of current user
		Set<Long> lastUserRetweetedUserIds = new HashSet<Long>(); // users whose tweets were retweet by the current user
		
		// Match the hashtag_...
		String lastHashTagInfoKey = null; 
		List<KeyValuePair<Long, Integer>> lastHashTagTweetIdsAndIndices = new Vector<KeyValuePair<Long, Integer>>();
		
		// Match the shutter_<userId>
		String lastShutterUserKey = null;
		long lastShutterUserId = -1;
		long lastUserShutterCount = 0;
		
		while(reader.hasNext()){
			String line = reader.next();
			if(line.isEmpty()){
				continue;
			}
			if(line.startsWith("tweet")){
				String[] data = line.split("\t");
				if(data[0].equals(lastTweetInfoKey)){
					continue; // Avoid duplication
				}

				if(data.length < 5 || data.length > 6){
					System.err.println(line);
					continue;
				}
				lastTweetInfoKey = data[0];
				
				// Score1: each tweet plus 1
				try {
					long userId = Long.parseLong(data[4]);
					writer.write(String.format("score_%d\t%d\t%d", userId, 1, 1));
					writer.newLine();
				} catch (NumberFormatException e) {
					System.err.println("NUMBER FORMAT:" + line);
					continue;
				}
				
				
				if(data.length > 5){
					if(data.length == 6){
						// The last element is the retweetee_id
						// Score 2: each retweet plus 3
						writer.write(String.format("score_%d\t%d\t%d", Long.parseLong(data[5]), 2, 3));
						writer.newLine();
						
						// Remove this part because it need not be shown in the tweet_*** line
						// line = line.substring(0, line.lastIndexOf('\t')); 
					}else{
						System.err.println("INVALID LINE:" + line);
					}
					
				}
				
				// Write the tweet info
				writer.write("tweet\t" + line.substring(6));
				writer.newLine();
			}else if(line.startsWith("retweet")){
				String[] data = line.split("\t"); 
				if(!data[0].equals(lastRetweetKey) && lastRetweetUserId > 0){
					// Last user is done
					// make sure he/she has retweeters, not just retweeted others' tweets
					if(lastUserRetweeterIds.size() > 0){
						writeRetweetLine(writer, lastRetweetUserId, lastUserRetweeterIds, lastUserRetweetedUserIds);
					}
					
					lastUserRetweeterIds.clear();
					lastUserRetweetedUserIds.clear();
				}
				
				lastRetweetUserId = Long.parseLong(data[0].substring(8)); // Remove the retweet_ prefix
				if(data.length == 3){
					assert data[2].equals("R");
					lastUserRetweetedUserIds.add(Long.parseLong(data[1]));
				}else{
					assert data.length == 2;
					lastUserRetweeterIds.add(Long.parseLong(data[1]));
				}
				lastRetweetKey = data[0];
				
			}else if(line.startsWith("hashtag")){
				String[] data = line.split("\t");
				String hashTagInfo = data[0];
				Long tweetId = Long.parseLong(data[1]);
				Integer tweetIndice = Integer.parseInt(data[2]);
				
				if(lastHashTagInfoKey != null && !hashTagInfo.equals(lastHashTagInfoKey)){
					// Write the result of last hashtag
					writeHashtagInfo(writer, lastHashTagInfoKey, lastHashTagTweetIdsAndIndices);
					lastHashTagTweetIdsAndIndices = new Vector<KeyValuePair<Long,Integer>>();
				}
				
				lastHashTagInfoKey = hashTagInfo;
				lastHashTagTweetIdsAndIndices.add(new KeyValuePair<Long, Integer>(tweetId, tweetIndice));
				
			}else if(line.startsWith("shutter_")){
				String[] data = line.split("\t");
				if(!data[0].equals(lastShutterUserKey)){
					if(lastShutterUserKey != null){
						writeShutterLine(writer, lastShutterUserId, lastUserShutterCount);
					}
					
					lastShutterUserKey = data[0];
					lastShutterUserId = Long.parseLong(lastShutterUserKey.substring(8));
					lastUserShutterCount = 0;
				}
				lastUserShutterCount += Long.parseLong(data[1]);
				
			}else{
				System.err.println("Invalid line: " + line + "....");
				// throw new EMRException("Invalid line prefix");
			}
		}
		
		// Make sure all data are flushed
		if(lastHashTagInfoKey != null && lastHashTagTweetIdsAndIndices.size() > 0){
			writeHashtagInfo(writer, lastHashTagInfoKey, lastHashTagTweetIdsAndIndices);
		}
		
		if(lastUserRetweeterIds.size() > 0){
			writeRetweetLine(writer, lastRetweetUserId, lastUserRetweeterIds, lastUserRetweetedUserIds);
		}
		
		if(lastShutterUserKey != null && lastUserShutterCount > 0){
			writeShutterLine(writer, lastShutterUserId, lastUserShutterCount);
		}
		
		writer.flush();
	}
	
	private static void writeShutterLine(BufferedWriter writer, long userId, long shutterCount) throws IOException{
		writer.write(String.format("shutter\t%d\t%d", userId, shutterCount));
		writer.newLine();
	}
	
	private static void writeRetweetLine(BufferedWriter writer, 
										 long userId, 
										 List<Long> retweeterIds, 
										 Set<Long> retweetedUserIds) throws IOException{
		// Sort retweeter ids
		Collections.sort(retweeterIds);
		
		StringBuffer buffer = new StringBuffer();
		
		long lastRetweeterId = -1;
		long uniqueRetweeterCount = 0;
		for(int i = 0; i < retweeterIds.size(); i++){
			if(retweeterIds.get(i).equals(lastRetweeterId)){
				continue; // Duplicate records, ignore
			}
			
			uniqueRetweeterCount += 1;
			if(i > 0)
				buffer.append("\\n");
			
			if(retweetedUserIds.contains(retweeterIds.get(i))){
				buffer.append('(');
				buffer.append(retweeterIds.get(i));
				buffer.append(')');
			}else{
				buffer.append(retweeterIds.get(i));
			}
			lastRetweeterId = retweeterIds.get(i);
		}
		
		writer.write(String.format("retweet\t%d\t", userId));
		writer.write(buffer.toString());
		writer.newLine();
		
		// Score3: 10 for each unique retweeter
		writer.write(String.format("score_%d\t%d\t%d", userId, 3, uniqueRetweeterCount * 10));
		writer.newLine();
	}
	
	private static void writeHashtagInfo(BufferedWriter writer, String hashTagInfo, List<KeyValuePair<Long, Integer>> tweetIdsAndIndices) throws IOException{
		String[] data = hashTagInfo.split("_");
		String dayString = data[1],
			   locationString = data[2];
		String hashTagName = data[3];
		if(data.length > 4){ // The hashtag contains '_'
			for(int i = 4; i < data.length; i++)
				hashTagName += "_" + data[i];
		}
		if(hashTagInfo.charAt(hashTagInfo.length() - 1) == '_' && 
			hashTagName.charAt(hashTagName.length() - 1) != '_'){
			hashTagName += "_";
		}
		
		Collections.sort(tweetIdsAndIndices, new Comparator<KeyValuePair<Long, Integer>>() {
			@Override
			public int compare(KeyValuePair<Long, Integer> o1,
					KeyValuePair<Long, Integer> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		
		List<KeyValuePair<Long, Integer>> duplicationRemovedTweetIdsAndIndices = new Vector<KeyValuePair<Long,Integer>>();
		for(int i = 0; i < tweetIdsAndIndices.size(); i++){
			if(duplicationRemovedTweetIdsAndIndices.size() == 0 || 
			   !duplicationRemovedTweetIdsAndIndices.get(duplicationRemovedTweetIdsAndIndices.size() - 1).getKey().equals(tweetIdsAndIndices.get(i).getKey())){
				duplicationRemovedTweetIdsAndIndices.add(tweetIdsAndIndices.get(i));
			}
		}
		tweetIdsAndIndices = null; // Release resources
		
		StringBuffer buffer = new StringBuffer();
		for(int i = 0; i < duplicationRemovedTweetIdsAndIndices.size(); i++){
			if(i > 0)
				buffer.append(',');
			buffer.append(duplicationRemovedTweetIdsAndIndices.get(i).getKey());
		}
		
		writer.write(String.format("hashtag\t%s\t%s\t%s\t%d\t%s\t%d", 
									dayString, locationString, hashTagName, 
									duplicationRemovedTweetIdsAndIndices.size(), 
									buffer.toString(), 
									duplicationRemovedTweetIdsAndIndices.get(0).getValue()));
		writer.newLine();
		
	}
	
}
