package emr;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;

public class MySQLLoaderFile {
	private static String ENCODING = "UTF-8";
	public static BufferedWriter getBufferedWriter(String fileName) throws UnsupportedEncodingException, FileNotFoundException{
		return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), ENCODING));
	}
	
	public static String toLegalTSVString(String string){
		return string.replace("\\", "\\\\").replace("\n", "\\n")
							.replace("\r", "\\r")
							.replace("\t", "\\t")
							.replace("\'", "\\\'");
	}
	
	public static void main(String[] args) throws IOException{
		String configFile = args[0];
		Map<String, String> configuration = new HashMap<String, String>();
		Scanner scanner = new Scanner(new File(configFile));
		scanner.useDelimiter("\n");
		while(scanner.hasNext()){
			String line = scanner.next();
			if(line.isEmpty() || line.startsWith("#"))
				continue;
			
			String[] data = line.split("=");
			configuration.put(data[0], data[1]);
		}
		scanner.close();
		String inputFilePath = configuration.get("inputFilePath"),
				tweetInfoFile = configuration.get("tweetFile"),
				retweetRelationFile = configuration.get("retweetFile"),
				hashTagFile = configuration.get("hashtagFile"),
				scoreFile   = configuration.get("scoreFile"),
				shutterCountFile = configuration.get("shutterFile");
		
		
		BufferedWriter retweetInfoWriter = null, 
				       hashTagWriter = null,
				       scoreWriter = null,
				       shutterWriter = null;
		List<BufferedWriter> tweetPartitionWriters = null; 
		
		// Init writers
		int partitionNum = 4;	
		if(tweetInfoFile != null){
			tweetPartitionWriters = new Vector<BufferedWriter>();
			
			for(int i = 0;i < partitionNum; i++){
				tweetPartitionWriters.add(getBufferedWriter(tweetInfoFile + i));
			}
		}
		
		if(hashTagFile != null){
			hashTagWriter = getBufferedWriter(hashTagFile);
		}
		
		if(retweetRelationFile != null){
			retweetInfoWriter = getBufferedWriter(retweetRelationFile);
		}
		
		if(scoreFile != null){
			scoreWriter = getBufferedWriter(scoreFile);
		}
		
		if(shutterCountFile != null){
			shutterWriter = getBufferedWriter(shutterCountFile);
		}
		
		// Collect files
		List<File> filesToProcess = new Vector<File>();
		File inputFile = new File(inputFilePath);
		if(inputFile.isFile()){
			filesToProcess.add(inputFile);
		}else if(inputFile.isDirectory()){
			for(File f: inputFile.listFiles()){
				filesToProcess.add(f);
			}
		}else{
			throw new FileNotFoundException(inputFile.getAbsolutePath());
		}
		
		for(File file: filesToProcess){
			System.out.println("Processing:" + file.getAbsolutePath());
			scanner = new Scanner(new InputStreamReader(new FileInputStream(file), ENCODING));
			scanner.useDelimiter("\n");
			
			while(scanner.hasNext()){
				String line = scanner.next();
				if(line.isEmpty())
					continue;
				
				if(line.startsWith("tweet")){
					if(tweetPartitionWriters == null){
						continue;
					}
					// col1:    tid
					// col2:    content
					// col3:    score
					// col4:    time
					// col5:    userId
					// col6:    raw
					assert line.startsWith("tweet\t");
					line = line.substring(6);
					
					line = line.replace(Utils.placeHolderForNewLine, "\n");
					String[] data = line.split("\t");
					long tid = Long.parseLong(data[0]);
					String cleanString = toLegalTSVString(data[1]);
					int score = Integer.parseInt(data[2]);
					String timeString = data[3];
					long userId = Long.parseLong(data[4]);
					
					// String rowKey = String.format("%d_%s_%d", userId, timeString.replaceAll("[:\\-]", "").replaceAll(" ", ""), tid);
					BufferedWriter tweetInfoWriter = tweetPartitionWriters.get((int)(userId % partitionNum));	
					tweetInfoWriter.write(String.format("%d\t%s\t%d\t%s\t%d", 
										  tid, cleanString, score, timeString, userId));
					tweetInfoWriter.newLine();
					tweetInfoWriter.flush();
					
				}else if(line.startsWith("retweet")){
					if(retweetInfoWriter == null)
						continue;
					
					String[] strings = line.split("\t");
					String userId = strings[1], retweeterIds = strings[2];
					
					retweetInfoWriter.write(String.format("%s\t%s", userId, retweeterIds));
					retweetInfoWriter.newLine();
					retweetInfoWriter.flush();
					
				}else if(line.startsWith("hashtag")){
					if(hashTagWriter == null)
						continue;
					
					String[] data = line.split("\t");
					String dayString = data[1],
						   location  = data[2],
						   hashTagName = data[3],
						   popularity  = data[4],
						   tweetIds    = data[5],
						   indice      = data[6];
					String firstTweetId = tweetIds;
					if(tweetIds.indexOf(',') > 0){
						firstTweetId = tweetIds.substring(0, tweetIds.indexOf(','));
					}
					hashTagWriter.write(String.format("%s\t%s\t%s\t%s\t%s\t%s\t%s", 
													  dayString, location, hashTagName, 
													  popularity, tweetIds, indice, firstTweetId));
					hashTagWriter.newLine();
					hashTagWriter.flush();
				}else if(line.startsWith("score_")){
					if(scoreWriter == null)
						continue;
					scoreWriter.write(line.substring(6));
					scoreWriter.newLine();
					scoreWriter.flush();
				}else if(line.startsWith("shutter\t")){
					if(shutterWriter == null)
						continue;
					
					shutterWriter.write(line.substring(8));
					shutterWriter.newLine();
					shutterWriter.flush();
				}
			}
			scanner.close();
		}
		
		
		if(hashTagWriter != null)
			hashTagWriter.close();
		
		if(retweetInfoWriter != null)
			retweetInfoWriter.close();
		
		if(tweetPartitionWriters != null){
			for(int i = 0; i < tweetPartitionWriters.size(); i++) tweetPartitionWriters.get(i).close();
		}
		
		if(scoreWriter != null) scoreWriter.close();
		
		if(shutterWriter != null) shutterWriter.close();
		
		
		
	}
}
