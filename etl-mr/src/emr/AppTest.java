package emr;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

public class AppTest {
	
	
	public static void main(String[] args) throws Exception{
		TweetMapper mapper = new TweetMapper();
		Scanner scanner = new Scanner(new File("data/temptest"));
		scanner.useDelimiter("\n");
		while(scanner.hasNext()){
			String line = scanner.next();
			ProcessedTweet tweet = mapper.process(line);
			System.out.println(tweet.cleanString);
			System.out.println(tweet.score);
		}
		
	}
}
