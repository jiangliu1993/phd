package emr;

public class Utils {
	public static final String ENCODING = "UTF-8";
	
	public static String placeHolderForNewLine = "<PHDNEWLINE>";
	public static final String BANLIST_FILE = "banned.txt";
	public static final String TERM_SCORE_FILE = "AFINN.txt";
}
