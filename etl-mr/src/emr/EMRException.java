package emr;

public class EMRException extends Exception{
	
	private String message;
	public EMRException(String message){
		super(message);
		this.message = message;
	}
}
