package emr;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

public class HBaseLoader {
	
	private static String ENCODING = "UTF-8";
	public static BufferedWriter getBufferedWriter(String fileName) throws UnsupportedEncodingException, FileNotFoundException{
		return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), ENCODING));
	}
	
	public static String toLegalTSVString(String string){
		return string.replace("\\", "\\\\").replace("\n", "\\n")
							.replace("\r", "\\r");
	}
	
	public static void main(String[] args) throws IOException{
		String inputFilePath = args[0],
				tweetInfoFile = args[1],
				retweetRelationFile = args[2],
				hashTagFile = args[3];
		
		List<File> filesToProcess = new Vector<File>();
		File inputFile = new File(inputFilePath);
		if(inputFile.isFile()){
			filesToProcess.add(inputFile);
		}else if(inputFile.isDirectory()){
			for(File f: inputFile.listFiles()){
				filesToProcess.add(f);
			}
		}
		
		BufferedWriter tweetInfoWriter = getBufferedWriter(tweetInfoFile),
				   	   retweetInfoWriter = getBufferedWriter(retweetRelationFile),
			           hashTagWriter   = getBufferedWriter(hashTagFile);
	
		for(File file: filesToProcess){
			System.out.println(file.getAbsolutePath());
			Scanner scanner = new Scanner(new InputStreamReader(new FileInputStream(file), ENCODING));
			
			scanner.useDelimiter("\n");
			
			while(scanner.hasNext()){
				String line = scanner.next();
				if(line.isEmpty())
					continue;
				
				if(line.startsWith("tweet")){
					// row-key: <userId>_<timestamp>
					// col1:    tweetId
					// col2:    content
					// col3:    score
					assert line.startsWith("tweet\t");
					line = line.substring(6);
					
					line = line.replace(Utils.placeHolderForNewLine, "\n");
					String[] data = line.split("\t");
					long tid = Long.parseLong(data[0]);
					String cleanString = toLegalTSVString(data[1]);
					int score = Integer.parseInt(data[2]);
					String timeString = data[3];
					long userId = Long.parseLong(data[4]);
					
					String rowKey = String.format("%d_%s", userId, timeString.replaceAll("[:\\-]", "").replaceAll(" ", ""));
					
					tweetInfoWriter.write(String.format("%s\t%d\t%s\t%d", rowKey, tid, cleanString, score));
					tweetInfoWriter.newLine();
				}else if(line.startsWith("retweet")){
					String[] strings = line.split("\t");
					String userId = strings[1], retweeterIds = strings[2];
					
					retweetInfoWriter.write(String.format("%s\t%s", userId, retweeterIds));
					retweetInfoWriter.newLine();
					retweetInfoWriter.flush();
					
				}else if(line.startsWith("hashtag")){
					String[] data = line.split("\t");
					String dayString = data[1],
						   location  = data[2],
						   hashTagName = data[3],
						   popularity  = data[4],
						   tweetIds    = data[5],
						   indice      = data[6];
					
					// rowKey: day_location_hashtag
					// col1  : popularity
					// col2  : tweetIds
					// col3  : indice
					// col4  : firstTweetId
					String rowKey = String.format("%s_%s_%s", dayString.replaceAll("\\-", ""), location, hashTagName);
					String firstTweetId = tweetIds;
					if(tweetIds.indexOf(',') > 0){
						firstTweetId = tweetIds.substring(0, tweetIds.indexOf(','));
					}
					
					hashTagWriter.write(String.format("%s\t%s\t%s\t%s\t%s", rowKey, popularity, tweetIds, indice, firstTweetId));
					hashTagWriter.newLine();
				}
			}
			
			scanner.close();
		}
		
		hashTagWriter.close();
		retweetInfoWriter.close();
		tweetInfoWriter.close();
		
		
	}
}
