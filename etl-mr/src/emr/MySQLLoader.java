package emr;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import org.json.simple.parser.ParseException;



public class MySQLLoader {
	String host;
	int    port;
	String username;
	String password;
	String dbName;
	Connection connection;
	String MYSQL_CHARSET = "utf8mb4";
	String tweetInfoTableName = "tweet";
	String retweetRelationTableName = "retweet";
	String hashTagTableName = "hashtag";
	
	public MySQLLoader(String host, int port, String username, String password, String dbName) throws SQLException{
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.dbName = dbName;
		
		String url = String.format("jdbc:mysql://%s:%d/%s?useUnicode=true&characterEncoding=UTF8", host, port, dbName);
		//String url = String.format("jdbc:mysql://%s:%d/%s?useUnicode=true", host, port, dbName);
        connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        
        // executeStatement(String.format("ALTER DATABASE phd CHARACTER SET '%s' COLLATE '%s_unicode_ci'", MYSQL_CHARSET, MYSQL_CHARSET));
        executeStatement(String.format("SET character_set_connection='%s'", MYSQL_CHARSET));
        executeStatement(String.format("SET character_set_server='%s'", MYSQL_CHARSET));
        executeStatement(String.format("SET character_set_client='%s'", MYSQL_CHARSET));
        
        Statement statement = connection.createStatement();
        statement.execute("show variables like 'char%'");
        ResultSet resultSet = statement.getResultSet();
        
        while(resultSet.next()){
        	String name = resultSet.getString(1),
        			charSet = resultSet.getString(2);
        	System.out.println(name + ": " + charSet);
        }
	}
	
	private void executeStatement(String cmd) throws SQLException{
		connection.createStatement().execute(cmd);
	}
	
	public void loadFile(String fileName) throws IOException, SQLException{
		System.out.println("Loading file: " + fileName);
		Scanner reader = new Scanner(new InputStreamReader(new FileInputStream(fileName), 
									Utils.ENCODING)); //new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
		reader.useDelimiter("\n");
		
		String line = null;
		int cnt = 0;
		int idx = 0;
		while(reader.hasNext()){
			line = reader.next();
			if(idx % 100000 == 0)
				System.out.println(idx + " " + line);
			idx += 1;
			
			
			try {
				String sql = null;
				
				if(line.startsWith("tweet")){
					ProcessedTweet result = parseTweetInfoResult(line);
					sql = String.format("INSERT INTO %s (tid, content, score, user_id, time) values(%d, '%s', %d, '%s', %d, '%s')"
												,tweetInfoTableName, result.tid, cleanSQLstr(result.cleanString), 
												result.score, 
												result.userId, result.timeString);
				}else if(line.startsWith("retweet")){
					String[] data = line.split("\t");
					String userId = data[1],
							retweeterId = data[2];
					
					sql = String.format("INSERT INTO %s (user_id, retweeter_id) values(%s, %s)"
											  ,retweetRelationTableName, userId, retweeterId);
					
					
				}else if(line.startsWith("hashtag")){
					String[] data = line.split("\t");
					String dayString = data[1],
						   location  = data[2],
						   hashTagName = data[3],
						   popularity  = data[4],
						   tweetIds    = data[5],
						   indice      = data[6];
					
					sql = String.format("INSERT INTO %s (day, location, hashtag, popularity, tweet_ids, indice) values('%s', '%s', '%s', %s, '%s', %s)"
							  ,hashTagTableName, dayString, cleanSQLstr(location), cleanSQLstr(hashTagName), popularity, tweetIds, indice);
				}
				
				connection.createStatement().executeUpdate(sql);
				cnt += 1;
				if(cnt > 100){
					connection.commit();
					cnt = 0;
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println(line);
				System.out.println(line.split("\t").length);
				System.exit(0);
			}
			
			
		}
		if(cnt > 0)
			connection.commit();
		
		reader.close();
	}
	
	public String cleanSQLstr(String string){
		return string.replace("\\", "\\\\").replace("\'", "\\\'");
	}
	
	public static ProcessedTweet parseTweetInfoResult(String line){
		assert line.startsWith("tweet\t");
		line = line.substring(6);
		
		line = line.replace(Utils.placeHolderForNewLine, "\n");
		String[] data = line.split("\t");
		long tid = Long.parseLong(data[0]);
		String cleanString = data[1];
		int score = Integer.parseInt(data[2]);
		String timeString = data[3];
		long userId = Long.parseLong(data[4]);
		
		ProcessedTweet result = new ProcessedTweet();
		result.tid = tid;
		result.cleanString = cleanString;
		result.score = score;
		result.timeString = timeString;
		result.userId = userId;
		
		return result;
	}
	
	public static void main(String[] args) throws NumberFormatException, SQLException, IOException, ParseException{
		
		MySQLLoader loader = new MySQLLoader(args[1], Integer.parseInt(args[2]), args[3], args[4], args[5]);
		
		if((new File(args[0])).isFile())
			loader.loadFile(args[0]);
		else if(new File(args[0]).isDirectory()){
			File[] files = new File(args[0]).listFiles();
			for(File file: files){
				loader.loadFile(file.getAbsolutePath());
			}
		}
		
//		Scanner scanner = new Scanner(new InputStreamReader(new FileInputStream("temptest"), TwitProcesser.ENCODING));
//		String line = scanner.nextLine();
//		JSONObject data = (JSONObject) new JSONParser().parse(line);
//		String content = (String) data.get("text");
//		
//		System.out.println(content);
		
		// loader.connection.createStatement().executeUpdate("insert into temp values('" + loader.clean_sqlstr(content) + "')");
		// loader.connection.commit();
	}
	
}
