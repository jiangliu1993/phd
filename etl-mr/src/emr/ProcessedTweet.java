package emr;

import java.util.List;
import java.util.Vector;

public class ProcessedTweet{
	/**
	 * Information related to the tweet itself
	 */
	public String cleanString;
	public int score;
	public long tid;
	public long userId;
	public String timeString;
	
	/**
	 * Information related to retweet
	 */
	public boolean isRetweet;
	public long originalUser = -1;
	
	/**
	 * Information related to hash tag
	 */
	String dayString;
	String location = null;
	List<String> hashTags = new Vector<String>();
	List<Long> hashTagIndices = new Vector<Long>();
	
	/**
	 * Information about shutters
	 */
	public long shutterCount = 0;
}
