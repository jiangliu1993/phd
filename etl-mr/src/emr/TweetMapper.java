package emr;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class TweetMapper {
	
	
	private Map<String, Integer> scoreMap = new java.util.Hashtable<String, Integer>();
	private Set<String> bannedTerms = new HashSet<String>();
	private DateFormat dateFormat;
	private DateFormat writeDateFormat;
	private DateFormat writeOnlyDayFormat;
	
	public TweetMapper() throws IOException{
		String string = null;
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				TweetMapper.class.getClassLoader().getResourceAsStream(Utils.TERM_SCORE_FILE)));
		
		while((string = reader.readLine()) != null){
			String[] data = string.split("\t");
			scoreMap.put(data[0], Integer.parseInt(data[1]));
		}
		
		reader.close();
		
		reader = new BufferedReader(new InputStreamReader(
							TweetMapper.class.getClassLoader().getResourceAsStream(Utils.BANLIST_FILE)));
		
		while((string = reader.readLine()) != null){
			// Decipher the term
			char[] chararray = string.toLowerCase().toCharArray();
			for(int i = 0; i < chararray.length; i++){
				char c = chararray[i];
				if(c <= 'm' && c >= 'a'){
					c += 13;
				}else if(c >= 'n' && c <= 'z'){
					c -= 13;
				}
				chararray[i] = c;
			}
			bannedTerms.add(new String(chararray));
		}
		
		reader.close();
		
		dateFormat = new SimpleDateFormat("yyyy MMM dd HH:mm:ss", Locale.US);
		writeDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		writeOnlyDayFormat = new SimpleDateFormat("yyyy-MM-dd");
	}
	
	
	
	private boolean isAlphaNumeric(char c){
		return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9');
	}
	
	public ProcessedTweet process(String jsonStr) throws Exception{
		ProcessedTweet result = new ProcessedTweet();
		
		// Read data from json string
		JSONParser parser = new JSONParser();
		JSONObject data = null;
		try {
			data = (JSONObject) parser.parse(jsonStr);
		} catch (Exception e) {
			System.err.println(jsonStr);
			throw e;
		}
		
		String content = (String) data.get("text");
		long tid       = (Long) data.get("id");
		long userId    = (Long) ((JSONObject)data.get("user")).get("id");
		String timeStr = (String) data.get("created_at");
		
		
		// Furthur process
		timeStr = timeStr.substring(4);
		String yearString = timeStr.substring(timeStr.lastIndexOf(' ') + 1);
		timeStr = timeStr.substring(0, timeStr.indexOf('+') - 1);
		
		Date tweetTime = dateFormat.parse(String.format("%s %s", yearString, timeStr));
		timeStr = writeDateFormat.format(tweetTime);
		String dayString = writeOnlyDayFormat.format(tweetTime);
		
		char[] charArr = content.toCharArray();
		
		int idx = 0;
		int score = 0;
		while(idx < content.length()){
			char c = content.charAt(idx);
			if(isAlphaNumeric(c)){
				int nextidx = idx + 1;
				while(nextidx < content.length() && isAlphaNumeric(content.charAt(nextidx)))
					nextidx += 1;
				
				String term = content.substring(idx, nextidx).toLowerCase();
				if(scoreMap.containsKey(term)){
					score += scoreMap.get(term);
				}
				
				if(bannedTerms.contains(term)){
					for(int i = idx + 1; i < nextidx - 1; i++)
						charArr[i] = '*';
				}
				
				idx = nextidx + 1;
				
			}else{
				idx += 1;
			}
		}
		
		// Tweet info
		result.tid = tid;
		result.userId = userId;
		result.timeString = timeStr;
		result.cleanString = new String(charArr);
		result.score = score;
		
		// retweet info
		if(data.get("retweeted_status") != null){
			result.isRetweet = true;
			result.originalUser = (Long) ((JSONObject)((JSONObject)data.get("retweeted_status")).get("user")).get("id");
		}
		
		//hash tag info
		result.dayString = dayString;
		
		// Try to find location in different fields
		result.location = null;
		if(data.get("place") != null){
			result.location = (String) ((JSONObject)data.get("place")).get("name");
		}
		if(result.location == null || result.location.isEmpty()){
			// Try to find in timezone
			String timezone = (String) ((JSONObject)data.get("user")).get("time_zone");
			if(timezone != null){
				// Before using this value, we need to check if it contains "\btime\b"
				if(!Pattern.matches(".*\\btime\\b.*", timezone.toLowerCase())){
					result.location = timezone;
				}
			}
		}
		
		// Read tags
		JSONArray tags = (JSONArray) ((JSONObject)data.get("entities")).get("hashtags");
		for(int i = 0; i < tags.size(); i++){
			JSONObject object = (JSONObject) tags.get(i);
			result.hashTags.add((String)object.get("text"));
			result.hashTagIndices.add((Long)((JSONArray)object.get("indices")).get(0));
		}
		
		// Read photo counts
		result.shutterCount = 0;
		JSONArray media = (JSONArray) ((JSONObject)data.get("entities")).get("media");
		if(media != null){ // The field "media" don't necessarily exist, so it might be null
			for(int i = 0; i < media.size(); i++){
				JSONObject object = (JSONObject) media.get(i);
				if("photo".equalsIgnoreCase((String)object.get("type"))){
					result.shutterCount += 1;
				}
			}
		}
		
		return result;
	}
	
//	public static String formatResult(ProcessedTweet result){
//		return (String.format("%d\t%s\t%d\t%s\t%d\t%s", 
//							  result.tid, result.cleanString, 
//							  result.score, result.timeString, 
//							  result.userId, result.rawString)).replace("\n", Utils.placeHolderForNewLine);
//	}
//	

	
	/**
	 * Mapper main function
	 * Read tweet json string as input
	 * Output in either of the following formats
	 * 1. tweet_<tweet_id>   cleanedString   score   timeString   userId   [<retweeted_user_id>]
	 * 2. retweet_<user_id>    <retweetee_id>
	 * 3  retweet_<retweeter_id>     <user_id>    [R]
	 * 4. hashtag_<day>_<localtion>_<hashtag_name>   tweet_id   index
	 * 5. shutter_<user_id>   <photo_count>
	 * Each tweet will introduce 1 type 1 output line, zero or more type 2/3 output lines
	 * @param args
	 * @throws org.json.simple.parser.ParseException
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception{
		TweetMapper processer = new TweetMapper();
		Date timeBefore = new Date();
		// BufferedReader reader = new BufferedReader();
		Scanner reader = new Scanner(new InputStreamReader(System.in, Utils.ENCODING));
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out, Utils.ENCODING));
		
		String string = null;
		while(reader.hasNextLine()){
			string = reader.nextLine();
			// System.err.println(string);
			if(string.isEmpty())
				continue;
			
			ProcessedTweet result = processer.process(string);
			
			writeTweetInfo(writer, result);
			writeRetweetInfo(writer, result);
			writeHashTagInfo(writer, result);
			writeShutterCount(writer, result);
		}
		
		writer.close();
		reader.close();
		
		Date timeAfter = new Date();
		System.err.println(timeAfter.getTime() - timeBefore.getTime());
		// System.out.println();
	}
	
	public static void writeTweetInfo(BufferedWriter writer, ProcessedTweet tweet) throws IOException{
		String line = (String.format("tweet_%d\t%s\t%d\t%s\t%d", 
						tweet.tid, 
						tweet.cleanString.replace("\t", "\\t").replace("\n", "\\n").replace("\r", "\\r"),
						tweet.score, 
						tweet.timeString, 
						tweet.userId));
		
		if(tweet.isRetweet){
			line += "\t" + tweet.originalUser;
		}
		writer.write(line);
		writer.newLine();
	}
	
	public static void writeRetweetInfo(BufferedWriter writer, ProcessedTweet tweet) throws IOException{
		if(tweet.isRetweet){
			assert tweet.originalUser > 0;
			writer.write(String.format("retweet_%d\t%d", tweet.originalUser, tweet.userId));
			writer.newLine();
			writer.write(String.format("retweet_%d\t%d\tR", tweet.userId, tweet.originalUser));
			writer.newLine();
		}
	}
	
	public static void writeHashTagInfo(BufferedWriter writer, ProcessedTweet tweet) throws IOException{
		if(tweet.hashTags.size() > 0 && tweet.location != null && !tweet.location.isEmpty()){
			// This tweet is valid
			assert tweet.hashTags.size() == tweet.hashTagIndices.size();
			for(int i = 0; i < tweet.hashTags.size(); i++){
				writer.write(String.format("hashtag_%s_%s_%s\t%d\t%d",
											tweet.dayString,
											tweet.location,
											tweet.hashTags.get(i),
											tweet.tid,
											tweet.hashTagIndices.get(i)));
				writer.newLine();
			}
		}
	}
	
	public static void writeShutterCount(BufferedWriter writer, ProcessedTweet tweet) throws IOException{
		if(tweet.shutterCount > 0){
			writer.write(String.format("shutter_%d\t%d", tweet.userId, tweet.shutterCount));
			writer.newLine();
		}
	}
}
