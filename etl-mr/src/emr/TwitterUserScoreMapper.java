package emr;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class TwitterUserScoreMapper {
	
	public static void main(String[] args) throws IOException{
		Scanner reader = new Scanner(new InputStreamReader(System.in, Utils.ENCODING));
		reader.useDelimiter("\n");
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out, Utils.ENCODING));
		
		String string = null;
		while(reader.hasNext()){
			string = reader.next();
			if(string.isEmpty())
				continue;
			
			if(!string.startsWith("score_") && !string.startsWith("shutter"))
				continue;
			
			writer.write(string);
			writer.newLine();
			writer.flush();
		}
		
		writer.close();
		reader.close();
	}
}
