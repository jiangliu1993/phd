import pika, sys, os
from etl import TwitterManager
from db import get_db_access_object
from config import TASK_QUEUE_NAME, BUCKET_NAME
from aws import get_s3_connection
import subprocess
import gzip

def run_process(cmd):
    ps = subprocess.Popen(cmd, shell=True)
    out, err = ps.communicate()

    return out, err

def consume_task_callback(ch, method, properties, body):
    """
    Read file from s3 and process
    """
    print "[x] Received message: %s" % body
    # 1. Extract information from message. Format: s3key|dbtype|dbconnection_str
    [s3key, dbtype, dbconnection_str] = body.split('|')

    if dbtype == "mysql":
        [endpoint, port, user, passwd, dbname] = dbconnection_str.split(",")
        dao = get_db_access_object(dbtype, endpoint, int(port), user, passwd, dbname)
    elif dbtype == "hbase":
        [endpoint, port] = dbconnection_str.split(",")
        dao = get_db_access_object(dbtype, endpoint, int(port))

    # Init db connection
    manager = TwitterManager(dao)

    # 2. Read file from s3, extract locally and read
    # s3key looks like: 600GB/<filename>
    conn = get_s3_connection()

    bucket = conn.get_bucket(BUCKET_NAME)
    keyobj = bucket.get_key(s3key)

    localfilename = os.path.split(s3key)[1]
    local_extracted_filename = localfilename[:-3]

    fp = open(localfilename, 'wb')
    keyobj.get_contents_to_file(fp)
    fp.close()

    print "Extracting gz file locally"
    #inF = gzip.GzipFile(localfilename, 'rb')
    #s = inF.read()
    #inF.close()

    #outF = file(local_extracted_filename, 'wb')
    #outF.write(s)
    #outF.close()
    run_process("gzip -f -d %s" %(localfilename))

    # 3. Start to process the file
    print "Start importing file %s" % local_extracted_filename
    manager.start_load_process()

    counter = 0
    for line in open(local_extracted_filename):
        if line[-1] == '\n': line = line[: -1]
        if not line: continue

        manager.load_twit(line)

        counter += 1
        if counter % 100000 == 0:
            print counter

    manager.finish_load_process()

    # 4. Clean up
    # os.remove(localfilename)
    os.remove(local_extracted_filename)

    print "[x] Process %s done" % body
    # This is important, tell server this task is done, and send me the next one
    ch.basic_ack(delivery_tag = method.delivery_tag)

def main():
    mq_server_config_file = "/etc/phd-mq.conf"

    if "-h" in sys.argv:
        print "Put <message_queue_server_address> in %s, then run python worker.py" %(mq_server_config_file)
        sys.exit(0)
    
    if not os.path.exists(mq_server_config_file):
        print "config file %s not exist" %(mq_server_config_file)
        sys.exit(-1)

    message_queue_host = open(mq_server_config_file).read().replace("\n", "")

    connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=message_queue_host))

    channel = connection.channel()
    
    channel.queue_declare(queue=TASK_QUEUE_NAME, durable=True)
    print ' [*] Waiting for messages. To exit press CTRL+C'

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(consume_task_callback,
                          queue=TASK_QUEUE_NAME)

    channel.start_consuming()

if __name__ == '__main__':
    main()
