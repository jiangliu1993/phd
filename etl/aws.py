
from config import AWSACCESS_KEY, AWSSECRET_KEY
from boto.s3.connection import S3Connection

def get_s3_connection():
	return S3Connection(AWSACCESS_KEY, AWSSECRET_KEY)