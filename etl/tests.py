# coding=utf-8

import unittest
from etl import TwitterManager
from db import get_db_access_object

class SimpleTestCase(unittest.TestCase):

	def setUp(self):
		self.manager = TwitterManager(get_db_access_object("mysql", "localhost", 3306, "root", "root", "phd"))

	def test_transform_func(self):
		cases = [
				(u"ejaculate.assho+schlong.schlongaa", "e*******e.a***o+s*****g.schlongaa", 0),
				(u".grief disruption", ".grief disruption", -4),
				(u"高円寺モーガンカフェが閉店するとのこと。16年来通い詰めた私としては残念としかいいようがない。定食の気軽さ、珈琲の美味しさでは高円寺NO.1だったのに。最終営業日は3月25､26日あたり。http://t.co/8R2Hgfng4s http://t.co/n3gnb6nXKf;",
				 "高円寺モーガンカフェが閉店するとのこと。16年来通い詰めた私としては残念としかいいようがない。定食の気軽さ、珈琲の美味しさでは高円寺NO.1だったのに。最終営業日は3月25､26日あたり。http://t.co/8R2Hgfng4s http://t.co/n3gnb6nXKf;",
				 -1),
				(u"RT @KaitidSEX_: สวัสดีผมไค#ฟิคติดSEX อยากติดเซ็กส์ให้อ่านฟิคแต่ถ้าอยากมีคู่ชีวิตก็ให้อ่านฟิคนั่นแหละครับ {RT&amp;talk};",
				 "RT @KaitidSEX_: สวัสดีผมไค#ฟิคติดS*X อยากติดเซ็กส์ให้อ่านฟิคแต่ถ้าอยากมีคู่ชีวิตก็ให้อ่านฟิคนั่นแหละครับ {RT&amp;talk};",
				 0)]

		for s, snew_truth, score_truth in cases:
			score, snew = self.manager.process_string(s)
			print score, snew

			self.assertTrue(score == score_truth and snew == snew_truth)

	def test_timestr(self):
		self.assertTrue(self.manager.parse_timestr("Fri Mar 21 15:10:30 +0000 2014"))


if __name__ == '__main__':
	unittest.main()