from config import SENTIMENT_FILE, BANNED_TERMS_FILE
import json
import random
from datetime import datetime
from dateutil.parser import parse
import types

def rot13restore(char):
	"""
	must be lower case to work well
	"""
	intval = ord(char)
	if intval <= ord('m') and intval >= ord('a'):
		intval += 13
	elif intval >= ord('n') and intval <= 'z':
		intval -= 13
	else:
		return char

	return chr(intval)



class TwitterManager:

	def __init__(self, dao):
		"""
		Init a manager instance:
		1. Load sentiment and banned terms 
		2. Initialize the correct db interface
		"""
		self.sentiment_score_map = {}
		self.banned_terms = set()

		# 1. Read sentiment scores
		for line in open(SENTIMENT_FILE):
			if line[-1] == '\n': line = line[: -1]
			if not line: continue

			[term, score] = line.split('\t')
			self.sentiment_score_map[term] = int(float(score))

		# 2. Read banned terms
		for line in open(BANNED_TERMS_FILE):
			if line[-1] == '\n': line = line[: -1]
			if not line: continue

			# Need to decipher
			term = "".join([rot13restore(c) for c in list(line)])
			self.banned_terms.add(term)

		#print self.banned_terms
		#print self.sentiment_score_map

		self.dao = dao

		self.tablename = "twit"

		self.cache 	   = [ None for i in range(random.randint(8000, 12000)) ] # Random number makes it less likely for different workers to commit at the same time
		self.cacheidx  = 0

	def flush_cache(self):
		if self.cacheidx:
			self.dao.batch_insert_twit(self.tablename, self.cache)
			self.cacheidx = 0

	def start_load_process(self, clear_first=False):
		if clear_first:
			self.dao.drop_table(self.tablename)
			self.dao.create_table(self.tablename)

		self.cacheidx = 0

	def finish_load_process(self):
		self.flush_cache()

	def process_string(self, content):
		"""
		return a tuple: (score, processed string)
		"""

		score = 0

		idx = 0
		lencontent = len(content)
		# delimiters = set(list(".,/;'[]\\`<>?:\"{}|)(*&^%$#@!~"))

		characterarray = list(content)
		while idx < lencontent:
			ch = content[idx]
			if not ch.isalnum():
				idx += 1
				continue

			nextidx = idx + 1
			while nextidx < lencontent and content[nextidx].isalnum():
				nextidx += 1

			# Got a new term
			term = content[idx: nextidx].lower()
			# print term

			if self.sentiment_score_map.has_key(term):
				score += self.sentiment_score_map[term]

			if term in self.banned_terms:
				# Operate on the character array instead of the string itself
				for i in range(idx + 1, nextidx - 1): characterarray[i] = '*'

			idx = nextidx + 1

		return score, "".join(characterarray)

	def parse_timestr(self, timestr):
		timestr = timestr[4: ]
		result = parse(timestr)
		return result

	def load_twit(self, rawdata, format="json"):
		data = json.loads(rawdata)

		tid, content = data['id'], data['text']
		created_at = data['created_at']

		create_time = self.parse_timestr(created_at)
		user_id = data['user']['id']

		# 1. Summarize sentiment scores
		if type(content) is types.UnicodeType:
			content = content.encode("utf-8") # Unicode type can not support correct tokenization

		score, processed_content = self.process_string(content)

		self.cache[self.cacheidx] = (tid, processed_content, score, user_id, create_time, content)
		self.cacheidx += 1

		if self.cacheidx >= len(self.cache):
			self.flush_cache()
