"""
This is the script used to distributed all file names as messages
"""
# coding=utf-8

import sys
import pika, json
from config import TASK_QUEUE_NAME, BUCKET_NAME
from aws import get_s3_connection
from db import get_db_access_object
from etl import TwitterManager

def main():
    if "-h" in sys.argv:
        print "Usage: python master.py <message_queue_host> <dbtype> <dbconnection_str>"
        print "message_queue_host: the host address of message queue server"
        print "dbtype: mysql or hbase"
        print "dbconnection_str: for mysql it's like 'host,port,username,password,dbname', for hbase it's like 'host,port'"
        sys.exit(0)
        
    """
    Retrieve all files on s3, distribute the file name as well as other parameters to message queue
    """
    [message_queue_host, dbtype, dbconnection_str] = sys.argv[1: ]

    # 1. First, we clear the db we want to process
    if dbtype == "mysql":
        [endpoint, port, user, passwd, dbname] = dbconnection_str.split(",")
        dao = get_db_access_object(dbtype, endpoint, int(port), user, passwd, dbname)
    elif dbtype == "hbase":
        [endpoint, port] = dbconnection_str.split(",")
        dao = get_db_access_object(dbtype, endpoint, int(port))

    manager = TwitterManager(dao)
    manager.start_load_process(True)

    # 2. Then we sent out messages, we have these files to process
    connection = pika.BlockingConnection(pika.ConnectionParameters(
                host=message_queue_host))

    channel = connection.channel()

    channel.queue_declare(queue=TASK_QUEUE_NAME, durable=True)

    s3conn = get_s3_connection()
    bucket = s3conn.get_bucket(BUCKET_NAME)

    cnt = 0
    for keyobj in bucket.list(prefix="600G"):
        if len(keyobj.name) < 10:
            continue
        message = "|".join([keyobj.name, dbtype, dbconnection_str])

        channel.basic_publish(exchange='',
                              routing_key=TASK_QUEUE_NAME,
                              body=message,
                              properties=pika.BasicProperties(
                                 delivery_mode = 2, # make message persistent
                              ))
        print " [x] Sent %r" % (message,)
        cnt += 1

    connection.close()

    print "Task distribution done. %d tasks are distributed" %(cnt)

if __name__ == '__main__':
    main()