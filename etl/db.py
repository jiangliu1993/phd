# coding=utf-8

"""
DB level access
"""
import MySQLdb, happybase
import types
import uuid
from datetime import datetime
import _mysql_exceptions

MYSQL_CHARSET = "utf8mb4"
def clean_sqlstr(string):
	return string.replace("\\", "\\\\").replace("\'", "\\\'")

def gettimestr(dt):
	return dt.strftime("%Y-%m-%d %H:%M:%S")

EPOCH = datetime(1970, 1, 1)
def timestamp_string(dt):
	return dt.strftime("%Y%m%d%H%M%S")
	# return (dt.replace(tzinfo=None) - EPOCH).total_seconds()

def check_data(tid, content, score, user_id, time, raw):
	assert type(tid) == types.IntType
	assert type(content) == types.StringType
	assert type(score) == types.IntType
	assert type(raw) == types.StringType
	assert type(user_id) == types.IntType
	assert isinstance(time, datetime)

class MySQLWrapper:
	
	def __init__(self, endpoint, port, user, passwd, dbname):
		self.mysqldb = MySQLdb.connect(host=endpoint, port=port, user=user, passwd=passwd, db=dbname, charset=MYSQL_CHARSET)
		self.mysqldb.cursor().execute("ALTER DATABASE `%s` CHARACTER SET '%s' COLLATE '%s_unicode_ci'" %(dbname, MYSQL_CHARSET, MYSQL_CHARSET))

	def drop_table(self, tablename):
		cursor = self.mysqldb.cursor()

		cursor.execute("DROP TABLE IF EXISTS %s" %(tablename))
		self.mysqldb.commit()

	def create_table(self, tablename):
		cursor = self.mysqldb.cursor()
		sql = "CREATE TABLE IF NOT EXISTS %s" %(tablename) + \
						"(`tid` BIGINT NOT NULL, content VARCHAR(1024) CHARACTER SET " + MYSQL_CHARSET + " COLLATE " + MYSQL_CHARSET + "_general_ci NOT NULL," + \
						"score INT NOT NULL, raw VARCHAR(1024) CHARACTER SET " + MYSQL_CHARSET + " COLLATE " + MYSQL_CHARSET + "_general_ci NOT NULL, " + \
						"user_id BIGINT NOT NULL, time DATETIME NOT NULL, PRIMARY KEY (tid))"

		cursor.execute(sql)

		self.mysqldb.commit()

	


	def batch_insert_twit(self, tablename, rowlist):
		cursor = self.mysqldb.cursor()
		for tid, content, score, user_id, time, raw in rowlist:
			check_data(tid=tid, content=content, score=score, user_id=user_id, time=time, raw=raw)

			sql = "INSERT INTO " + tablename + " (tid, content, score, raw, user_id, time) values(%d, '%s', %d, '%s', %d, '%s')" \
							%(tid, clean_sqlstr(content), score, clean_sqlstr(raw), user_id, gettimestr(time))
			
			try:
				cursor.execute(sql)
			except  _mysql_exceptions.IntegrityError as e:
				# Duplicate Twitter Id, ignore it
				continue
			except Exception as e:
				print sql
				self.mysqldb.commit()
				raise e

		self.mysqldb.commit()

	# def insert_twit(self, tablename, tid, content, score, user_id, timestamp, raw):
	# 	"""
	# 	tid: twit id, in str, represent a big int
	# 	content: sensored content
	# 	score: float
	# 	raw: original unsensored content
	# 	"""

		

	# 	sql = "INSERT INTO " + tablename + " (tid, content, score, raw) values(%s, '%s', %f, '%s')" \
	# 						%(tid, clean_sqlstr(content), score, clean_sqlstr(raw))
	# 	cursor = self.mysqldb.cursor()
	# 	try:
	# 		cursor.execute(sql)
	# 		self.mysqldb.commit()
	# 	except Exception as e:
	# 		print sql
	# 		raise e

	def count_twit(self, tablename):
		sql = "select count(*) from %s";
		cursor = self.mysqldb.cursor()
		cursor.execute(sql)
		return cursor.fetchone[0]


class HBaseWrapper:

	def __init__(self, endpoint, port=None, user=None, passwd=None):
		self.connection = happybase.Connection(endpoint)

	def create_table(self, tablename):
		if tablename not in self.connection.tables():
			self.connection.create_table(tablename, {'phd': {}})

	def drop_table(self, tablename):
		if tablename in self.connection.tables():
			self.connection.delete_table(tablename, True)

	def batch_insert_twit(self, tablename, rowlist):
		# Since twit id is not unique and cannot be used a primary key, we use random string as the key
		table = self.connection.table(tablename)
		uniquekey = str(uuid.uuid1())
		batch = table.batch()
		for tid, content, score, user_id, time, raw in rowlist:
			check_data(tid=tid, content=content, score=score, user_id=user_id, time=time, raw=raw)
			# Use <userid>_<timestamp> as row key
			# content, score, raw, tid as columns
			rowkey = "%d_%s_%d" %(user_id, timestamp_string(time), tid)

			batch.put(rowkey, {'phd:tid': str(tid), "phd:content": content, "phd:score": str(score), "phd:raw": raw})

			# batch.put(uniquekey, {'tid': row[0], 'content': row[1], 'score': row[2], 'raw': row[3]})
		batch.send()

	def count_twit(self, tablename):
		table = self.connection.table(tablename)

def get_db_access_object(storage_name, endpoint, port=None, user=None, passwd=None, dbname=None):
	storage_name = storage_name.lower()
	if storage_name == "mysql":
		if not globals().has_key("mysql_wrapper"):
			globals()['mysql_wrapper'] = MySQLWrapper(endpoint, port, user, passwd, dbname)
		return globals()['mysql_wrapper']
	elif storage_name == "hbase":
		if not globals().has_key('hbase_wrapper'):
			globals()['hbase_wrapper'] = HBaseWrapper(endpoint)
		return globals()['hbase_wrapper']
	else:
		raise Exception("Unsupported storage_name")








