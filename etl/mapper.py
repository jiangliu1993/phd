#!/usr/bin/python

import os, sys
from etl import TwitterManager

def main():
	manager = TwitterManager()
	manager.start_load_process()

	counter = 0
	for line in sys.stdin:
		if line[-1] == '\n': line = line[: -1]
		if not line: continue

		manager.load_twit(line)

		counter += 1
		if counter % 1000 == 0:
			print counter

	manager.finish_load_process()

if __name__ == '__main__':
	main()
