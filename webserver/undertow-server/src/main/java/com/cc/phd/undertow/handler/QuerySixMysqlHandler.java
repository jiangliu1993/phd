package com.cc.phd.undertow.handler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Deque;
import java.util.Map;

import javax.sql.DataSource;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class QuerySixMysqlHandler implements HttpHandler {

	static final String teamInfo = "PermanentHeadDamage,1563-6721-1410,1421-8919-2387,9620-2216-5685\n";

	DataSource mysql;

	public QuerySixMysqlHandler(DataSource mysql) {
		this.mysql = mysql;
	}

	public void handleRequest(HttpServerExchange exchange) throws Exception {
		String result = teamInfo;
		Map<String, Deque<String>> query = exchange.getQueryParameters();

		if (query.get("m") == null || query.get("n") == null) {
			exchange.getResponseSender().send(result);
			return;
		}

		String lower = query.get("m").peek(), upper = query.get("n").peek();

		if (lower == null || upper == null) {
			exchange.getResponseSender().send(result);
			return;
		}

		/* Should not block IO threads */
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		long lowerCount = 0, upperCount = 0;

		try (Connection connection = mysql.getConnection()) {
			String lowerSql = "SELECT shutter_sum FROM shutter_sum WHERE user_id < ? ORDER BY user_id DESC LIMIT 1";
			try (PreparedStatement statement = connection.prepareStatement(
					lowerSql, ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY)) {
				statement.setString(1, lower);
				try (ResultSet resultSet = statement.executeQuery()) {
					if (!resultSet.next()) {
						lowerCount = 0;
					} else {
						lowerCount = resultSet.getLong(1);
					}
				}
			}
			String upperSql = "SELECT shutter_sum FROM shutter_sum WHERE user_id <= ? ORDER BY user_id DESC LIMIT 1";
			try (PreparedStatement statement = connection.prepareStatement(
					upperSql, ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY)) {
				statement.setString(1, upper);
				try (ResultSet resultSet = statement.executeQuery()) {
					if (!resultSet.next()) {
						result += 0 + "\n";
						exchange.getResponseSender().send(result);
						return;
					} else {
						upperCount = resultSet.getLong(1);
					}
				}
			}
		}

		result += (upperCount > lowerCount ? upperCount - lowerCount
				: lowerCount - upperCount) + "\n";
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
		exchange.getResponseSender().send(result);
	}
}
