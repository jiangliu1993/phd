package com.cc.phd.undertow.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.cc.phd.undertow.handler.QueryOneHandler;

public class ClockThread extends Thread {

	@Override
	public void run() {
		Calendar cal = null;
		SimpleDateFormat sdf = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss\n\n");
		while (true) {
			cal = Calendar.getInstance();
			QueryOneHandler.time = sdf.format(cal.getTime());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
	}
}
