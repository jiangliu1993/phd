package com.cc.phd.undertow.handler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import javax.sql.DataSource;

import com.cc.phd.undertow.utils.Hashtag;
import com.cc.phd.undertow.utils.HashtagQuery;
import com.google.common.cache.LoadingCache;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class QueryFourMysqlHandler implements HttpHandler {

	static final String teamInfo = "PermanentHeadDamage,1563-6721-1410,1421-8919-2387,9620-2216-5685\n";

	LoadingCache<HashtagQuery, LinkedList<Hashtag>> hashtagCache;
	DataSource mysql;

	public QueryFourMysqlHandler(
			LoadingCache<HashtagQuery, LinkedList<Hashtag>> hashtagCache,
			DataSource mysql) {
		this.hashtagCache = hashtagCache;
		this.mysql = mysql;
	}

	public void handleRequest(HttpServerExchange exchange) throws Exception {
		String result = teamInfo;
		Map<String, Deque<String>> query = exchange.getQueryParameters();
		Deque<String> date_values = query.get("date");
		if (date_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String date_value = date_values.peek();
		if (date_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		Deque<String> location_values = query.get("location");
		if (location_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String location_value = location_values.peek();
		if (location_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		Deque<String> m_values = query.get("m");
		if (m_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String m_value = m_values.peek();
		if (m_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		Deque<String> n_values = query.get("n");
		if (n_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String n_value = n_values.peek();
		if (n_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		/* Should not block IO threads */
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		HashtagQuery hquery = new HashtagQuery(date_value, location_value, Integer.parseInt(m_value), Integer.parseInt(n_value));
		LinkedList<Hashtag> results = hashtagCache.get(hquery);
		if (results.size() == 0) {
			exchange.getResponseSender().send(result);
			return;
		}
		int first_rank = results.getFirst().rank;
		int last_rank = results.getLast().rank;
		if (first_rank > hquery.m || last_rank < hquery.n) {
			// Cache not enough
			String sql = "SELECT hashtag, tweet_ids FROM hashtag WHERE day = ? AND location = ? ORDER BY popularity DESC, first_tweet_id ASC, indice ASC LIMIT ?,?";
			try (Connection connection = mysql.getConnection();
					PreparedStatement statement = connection.prepareStatement(
							sql, ResultSet.TYPE_FORWARD_ONLY,
							ResultSet.CONCUR_READ_ONLY)) {
				if (first_rank > hquery.m) {
					statement.setString(1, hquery.date);
					statement.setString(2, hquery.location);
					statement.setInt(3, hquery.m - 1);
					statement.setInt(4, first_rank - hquery.m);
					try (ResultSet resultSet = statement.executeQuery()) {
						LinkedList<Hashtag> before = new LinkedList<Hashtag>();
						int rank = hquery.m;
						while (resultSet.next()) {
							before.add(new Hashtag(resultSet.getString(1),
									resultSet.getString(2), rank));
							rank++;
						}
						results.addAll(0, before);
					}
				}
				if (last_rank < hquery.n) {
					statement.setString(1, hquery.date);
					statement.setString(2, hquery.location);
					statement.setInt(3, last_rank);
					statement.setInt(4, hquery.n - last_rank);
					try (ResultSet resultSet = statement.executeQuery()) {
						LinkedList<Hashtag> after = new LinkedList<Hashtag>();
						int rank = last_rank + 1;
						while (resultSet.next()) {
							after.add(new Hashtag(resultSet.getString(1),
									resultSet.getString(2), rank));
							rank++;
						}
						results.addAll(after);
					}
				}
			}
			hashtagCache.put(hquery, results);
		}
		for (Iterator<Hashtag> iterator = results.iterator(); iterator.hasNext();) {
			Hashtag hashtag = (Hashtag) iterator.next();
            if (hashtag.rank >= hquery.m && hashtag.rank <= hquery.n) {
                result += hashtag.tag + ":" + hashtag.tweetids + "\n";
            } else if (hashtag.rank > hquery.n) {
                break;
            }
		}
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
		exchange.getResponseSender().send(result);
	}
}
