package com.cc.phd.undertow.utils;

public final class Hashtag {
	public String tag;
	public String tweetids;
	public int rank;
	public Hashtag(String tag, String tweetids, int rank) {
		this.tag = tag;
		this.tweetids = tweetids;
		this.rank = rank;
	}
}