package com.cc.phd.undertow.handler;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Deque;
import java.util.Map;

import javax.sql.DataSource;

public class QueryTwoMysqlHandler implements HttpHandler {

	static final String teamInfo = "PermanentHeadDamage,1563-6721-1410,1421-8919-2387,9620-2216-5685\n";

	DataSource mysql;

	public QueryTwoMysqlHandler(DataSource mysql) {
		this.mysql = mysql;
	}

	public void handleRequest(HttpServerExchange exchange) throws Exception {
		String result = teamInfo;
		Map<String, Deque<String>> query = exchange.getQueryParameters();
		Deque<String> userid_values = query.get("userid");
		if (userid_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String userid_value = userid_values.peek();
		if (userid_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		Deque<String> tweet_time_values = query.get("tweet_time");
		if (tweet_time_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String tweet_time_value = tweet_time_values.peek();
		if (tweet_time_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		/* Should not block IO threads */
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		long userid = Long.parseLong(userid_value);
		String sql = "SELECT tid, score, content FROM tweet" + (userid % 4)
				+ " WHERE user_id = ? AND time = ? ORDER BY tid";
		try (Connection connection = mysql.getConnection();
				PreparedStatement statement = connection
						.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_READ_ONLY)) {
			statement.setLong(1, userid);
			statement.setString(2, tweet_time_value);
			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					result += resultSet.getLong(1) + ":" + resultSet.getInt(2)
							+ ":" + resultSet.getString(3) + "\n";
				}
			}
		}
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
		exchange.getResponseSender().send(result);
	}

}
