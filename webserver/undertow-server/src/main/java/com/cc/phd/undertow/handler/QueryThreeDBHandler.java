package com.cc.phd.undertow.handler;

import java.util.Deque;
import java.util.Map;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

import com.google.common.cache.LoadingCache;

public class QueryThreeDBHandler implements HttpHandler {

	static final String teamInfo = "PermanentHeadDamage,1563-6721-1410,1421-8919-2387,9620-2216-5685\n";
	
	private final LoadingCache<Long, String> friendsCache;
	
	public QueryThreeDBHandler(LoadingCache<Long, String> friendsCache) {
		this.friendsCache = friendsCache;
	}

	public void handleRequest(HttpServerExchange exchange) throws Exception {
		String result = teamInfo;
		Map<String, Deque<String>> query = exchange.getQueryParameters();
		Deque<String> userid_values = query.get("userid");
		if (userid_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String userid_value = userid_values.peek();
		if (userid_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		/* Should not block IO threads */
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		result += friendsCache.get(new Long(userid_value)) + "\n";
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
		exchange.getResponseSender().send(result);
	}

}
