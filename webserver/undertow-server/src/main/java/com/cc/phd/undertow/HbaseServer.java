package com.cc.phd.undertow;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;

import java.util.LinkedList;
import java.util.Properties;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.xnio.Options;

import com.cc.phd.undertow.handler.QueryOneHandler;
import com.cc.phd.undertow.handler.QueryTwoDBHandler;
import com.cc.phd.undertow.utils.Tweet;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class HbaseServer {

	final static TableName tableName = TableName.valueOf("phdtweet");
	final static byte[] family = Bytes.toBytes("phdcf");
	final static byte[] tweetID = Bytes.toBytes("tweetID");
	final static byte[] score = Bytes.toBytes("score");
	final static byte[] content = Bytes.toBytes("content");

	public static void startHbaseServer(Properties properties) throws Exception {
		final Configuration config = HBaseConfiguration.create();
		config.set("hbase.zookeeper.quorum",
				properties.getProperty("hbase.zookeeper.quorum"));
		final Connection conn = ConnectionFactory.createConnection(config);
		final LoadingCache<String, LinkedList<Tweet>> tweetCache = CacheBuilder
				.newBuilder().maximumSize(5000000)
				.build(new CacheLoader<String, LinkedList<Tweet>>() {
					@Override
					public LinkedList<Tweet> load(String query)
							throws Exception {
						try (Table table = conn.getTable(tableName)) {
							String temp[] = query.split("\t");
							temp[1] = temp[1].replaceAll("[^\\d]", "");
							Get get = new Get(Bytes.toBytes(temp[0] + "_"
									+ temp[1]));
							Result result = table.get(get);
							Tweet tweet = new Tweet(
									Long.parseLong(new String(result.getValue(
											family, tweetID), "UTF-8")),
									Integer.parseInt(new String(result
											.getValue(family, score), "UTF-8")),
									StringEscapeUtils.unescapeJava(new String(
											result.getValue(family, content),
											"UTF-8")));
							LinkedList<Tweet> results = new LinkedList<Tweet>();
							results.add(tweet);
							return results;
						}
					}
				});
		Undertow.builder()
				.addHttpListener(
						Integer.parseInt(properties.getProperty("server.port")),
						"0.0.0.0")
				.setBufferSize(1024 * 16)
				.setIoThreads(Runtime.getRuntime().availableProcessors() * 2)
				.setSocketOption(Options.BACKLOG, 10000)
				.setServerOption(UndertowOptions.ALWAYS_SET_KEEP_ALIVE, false)
				.setServerOption(UndertowOptions.ALWAYS_SET_DATE, true)
				.setHandler(
						Handlers.path()
								.addExactPath("/q1", new QueryOneHandler())
								.addExactPath("/q2",
										new QueryTwoDBHandler(tweetCache)))
				.setWorkerThreads(200).build().start();
	}
}
