package com.cc.phd.undertow.utils;

public final class Tweet {
	public long tid;
	public int score;
	public String content;
	
	public Tweet(long tid, int score, String content) {
		this.tid = tid;
		this.score = score;
		this.content = content;
	}
}
