package com.cc.phd.undertow;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;
import org.xnio.Options;

import com.cc.phd.undertow.handler.QueryFiveMysqlHandler;
import com.cc.phd.undertow.handler.QueryFourCacheHandler;
import com.cc.phd.undertow.handler.QueryOneHandler;
import com.cc.phd.undertow.handler.QuerySixMysqlHandler;
import com.cc.phd.undertow.handler.QueryThreeCacheHandler;
import com.cc.phd.undertow.handler.QueryTwoMysqlHandler;

public class MixServer {

	static DataSource newDataSource(String uri, String user, String password) {
		GenericObjectPool connectionPool = new GenericObjectPool();
		connectionPool.setMaxActive(256);
		connectionPool.setMaxIdle(256);
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
				uri, user, password);
		new PoolableConnectionFactory(connectionFactory, connectionPool, null,
				null, false, true);
		return new PoolingDataSource(connectionPool);
	}

	public static void startMixServer(Properties properties)
			throws Exception {
		DB db = DBMaker.newMemoryDirectDB().transactionDisable().make();
		Map<String, String> friendsCache = db.createHashMap("friends")
				.keySerializer(Serializer.STRING)
				.valueSerializer(Serializer.STRING).make();
		try (BufferedReader br = new BufferedReader(new FileReader(
				"retweet_post"))) {
			int count = 0;
			for (String line; (line = br.readLine()) != null;) {
				String[] temp = line.split("\t");
				friendsCache.put(temp[0], temp[1].replace("\\n", "\n"));
				count++;
				if (count % 100000 == 0) {
					System.out.println("Load: " + count);
				}
			}
			System.out.println("Cache load done");
		}
		Map<String, String[]> hashtagsCache = db.createHashMap("hashtag")
				.keySerializer(Serializer.STRING).make();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream("hashtag_post"), "UTF8"))) {
			int count = 0;
			for (String line; (line = br.readLine()) != null;) {
				String[] temp = line.split("\t");
				int len = Integer.parseInt(temp[2]);
				String[] hashtags = new String[len];
				for (int i = 0; i < len; i++) {
					hashtags[i] = br.readLine().replace("\t", ":");
					count++;
					if (count % 100000 == 0) {
						System.out.println("Load: " + count);
					}
				}
				hashtagsCache.put(temp[0] + "\t" + temp[1], hashtags);
			}
			System.out.println("Cache load done");
		}
		final DataSource mysql = newDataSource(
				properties.getProperty("mysql.uri"),
				properties.getProperty("mysql.user"),
				properties.getProperty("mysql.password"));
		Undertow.builder()
				.addHttpListener(
						Integer.parseInt(properties.getProperty("server.port")),
						"0.0.0.0")
				.setBufferSize(1024 * 16)
				.setIoThreads(Runtime.getRuntime().availableProcessors() * 2)
				.setSocketOption(Options.BACKLOG, 10000)
				.setServerOption(UndertowOptions.ALWAYS_SET_KEEP_ALIVE, false)
				.setServerOption(UndertowOptions.ALWAYS_SET_DATE, true)
				.setHandler(
						Handlers.path()
								.addExactPath("/q1", new QueryOneHandler())
								.addExactPath("/q2",
										new QueryTwoMysqlHandler(mysql))
								.addExactPath(
										"/q3",
										new QueryThreeCacheHandler(friendsCache))
								.addExactPath(
										"/q4",
										new QueryFourCacheHandler(hashtagsCache))
								.addExactPath("/q5",
										new QueryFiveMysqlHandler(mysql))
								.addExactPath("/q6",
										new QuerySixMysqlHandler(mysql)))
				.setWorkerThreads(200).build().start();
	}
}
