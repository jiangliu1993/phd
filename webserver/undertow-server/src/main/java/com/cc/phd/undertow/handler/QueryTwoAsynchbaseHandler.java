package com.cc.phd.undertow.handler;

import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.Map;

import org.hbase.async.GetRequest;
import org.hbase.async.HBaseClient;
import org.hbase.async.KeyValue;

import com.stumbleupon.async.Callback;
import com.stumbleupon.async.Deferred;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class QueryTwoAsynchbaseHandler implements HttpHandler {
	
	static final String teamInfo = "PermanentHeadDamage,1563-6721-1410,1421-8919-2387,9620-2216-5685\n";

	HBaseClient client;
	
	public QueryTwoAsynchbaseHandler(HBaseClient client) {
		this.client = client;
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		String result = teamInfo;
		Map<String, Deque<String>> query = exchange.getQueryParameters();
		Deque<String> userid_values = query.get("userid");
		if (userid_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String userid_value = userid_values.peek();
		if (userid_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		Deque<String> tweet_time_values = query.get("tweet_time");
		if (tweet_time_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String tweet_time_value = tweet_time_values.peek();
		if (tweet_time_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		/* Should not block IO threads */
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		tweet_time_value = tweet_time_value.replaceAll("[^\\d]", "");
		GetRequest get = new GetRequest("phdtweet", userid_value + "_" + tweet_time_value);
		Deferred<ArrayList<KeyValue>> key_values = client.get(get);
		Callback<Object, ArrayList<KeyValue>> cb  = new Callback<Object, ArrayList<KeyValue>> () {
			public Object call(ArrayList<KeyValue> arg) throws Exception {
				for (Iterator<KeyValue> iterator = arg.iterator(); iterator
						.hasNext();) {
					KeyValue kv = (KeyValue) iterator.next();
				}
				return null;
			}
		};
		key_values.addCallback(cb);
	}

}
