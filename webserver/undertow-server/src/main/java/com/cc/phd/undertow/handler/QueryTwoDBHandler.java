package com.cc.phd.undertow.handler;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import com.cc.phd.undertow.utils.Tweet;
import com.google.common.cache.LoadingCache;

public class QueryTwoDBHandler implements HttpHandler {

	static final String teamInfo = "PermanentHeadDamage,1563-6721-1410,1421-8919-2387,9620-2216-5685\n";
	
	private final LoadingCache<String, LinkedList<Tweet>> tweetCache;

	public QueryTwoDBHandler(
			LoadingCache<String, LinkedList<Tweet>> tweetCache) {
		this.tweetCache = tweetCache;
	}


	public void handleRequest(HttpServerExchange exchange) throws Exception {
		String result = teamInfo;
		Map<String, Deque<String>> query = exchange.getQueryParameters();
		Deque<String> userid_values = query.get("userid");
		if (userid_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String userid_value = userid_values.peek();
		if (userid_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		Deque<String> tweet_time_values = query.get("tweet_time");
		if (tweet_time_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String tweet_time_value = tweet_time_values.peek();
		if (tweet_time_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		/* Should not block IO threads */
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		LinkedList<Tweet> results = tweetCache.get(userid_value + "\t" + tweet_time_value);
		for (Iterator<Tweet> iterator = results.iterator(); iterator.hasNext();) {
			Tweet tweet = (Tweet) iterator.next();
			result += tweet.tid + ":" + tweet.score + ":" + tweet.content + "\n";
		}
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
		exchange.getResponseSender().send(result);
	}

}
