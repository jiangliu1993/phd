package com.cc.phd.undertow.utils;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class HashtagQuery {
	public String date;
	public String location;
	public int m;
	public int n;

	public HashtagQuery(String date, String location, int m, int n) {
		super();
		this.date = date;
		this.location = location;
		if (m < 1) {
			this.m = 1;
		} else {
			this.m = m;
		}
		this.n = n;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(date).append(location).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof HashtagQuery))
			return false;
		if (obj == this)
			return true;
		return new EqualsBuilder().append(date, ((HashtagQuery) obj).date)
				.append(location, ((HashtagQuery) obj).location).isEquals();
	}
}
