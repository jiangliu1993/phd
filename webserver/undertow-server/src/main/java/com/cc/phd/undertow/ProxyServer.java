package com.cc.phd.undertow;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import io.undertow.server.handlers.proxy.LoadBalancingProxyClient;
import io.undertow.server.handlers.proxy.ProxyHandler;

import java.net.URI;
import java.util.Properties;

import org.xnio.Options;

import com.cc.phd.undertow.handler.QueryOneHandler;

public class ProxyServer {
	public static void startProxyServer(Properties properties) throws Exception {
		LoadBalancingProxyClient clients = new LoadBalancingProxyClient();
		String backends = properties.getProperty("backends");
		String[] temp = backends.split(",");
		for (String host : temp) {
			clients.addHost(new URI(host));
		}
		Undertow.builder()
				.addHttpListener(
						Integer.parseInt(properties.getProperty("server.port")),
						"0.0.0.0")
				.setBufferSize(1024 * 16)
				.setIoThreads(Runtime.getRuntime().availableProcessors() * 2)
				.setSocketOption(Options.BACKLOG, 10000)
				.setServerOption(UndertowOptions.ALWAYS_SET_KEEP_ALIVE, false)
				.setServerOption(UndertowOptions.ALWAYS_SET_DATE, true)
				.setHandler(
						Handlers.path()
								.addExactPath("/q1", new QueryOneHandler())
								.addExactPath("/q2",
										new ProxyHandler(clients, null))
								.addExactPath("/q3",
										new ProxyHandler(clients, null))
								.addExactPath("/q4",
										new ProxyHandler(clients, null))
								.addExactPath("/q5",
										new ProxyHandler(clients, null))
								.addExactPath("/q6",
										new ProxyHandler(clients, null)))
				.setWorkerThreads(200).build().start();
	}
}
