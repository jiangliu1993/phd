package com.cc.phd.undertow.handler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.sql.DataSource;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class QueryFiveMysqlHandler implements HttpHandler {

	static final String teamInfo = "PermanentHeadDamage,1563-6721-1410,1421-8919-2387,9620-2216-5685\n";

	private class UserScoreStatistic {
		long userId;
		long score1;
		long score2;
		long score3;
		long scoreAll;

		public UserScoreStatistic(long uid, long s1, long s2, long s3, long sa) {
			userId = uid;
			score1 = s1;
			score2 = s2;
			score3 = s3;
			scoreAll = sa;
		}

		long getScore(int idx) {
			if (idx == 1)
				return score1;
			else if (idx == 2)
				return score2;
			else if (idx == 3)
				return score3;
			else
				return scoreAll;

		}
	}

	private DataSource mysql;

	public QueryFiveMysqlHandler(DataSource mysql) {
		this.mysql = mysql;
	}

	private UserScoreStatistic getUserScore(Connection connection, String userId)
			throws Exception {

		UserScoreStatistic result = null;
		String sql = "SELECT user_id, score1, score2, score3, score_all FROM user_score WHERE user_id=?";
		try (PreparedStatement statement = connection.prepareStatement(sql,
				ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
			statement.setString(1, userId);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next()) {
					result = new UserScoreStatistic(resultSet.getLong(1),
							resultSet.getLong(2), resultSet.getLong(3),
							resultSet.getLong(4), resultSet.getLong(5));
				}
			}
		}
		return result;
	}

	public void handleRequest(HttpServerExchange exchange) throws Exception {
		String result = teamInfo;
		Map<String, Deque<String>> query = exchange.getQueryParameters();

		if (query.get("m") == null || query.get("n") == null) {
			exchange.getResponseSender().send(result);
			return;
		}

		String userIdA = query.get("m").peek(), userIdB = query.get("n").peek();

		if (userIdA == null || userIdB == null) {
			exchange.getResponseSender().send(result);
			return;
		}

		/* Should not block IO threads */
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		try (Connection connection = mysql.getConnection()) {

			UserScoreStatistic userA = getUserScore(connection, userIdA);
			UserScoreStatistic userB = getUserScore(connection, userIdB);
			if (userA == null || userB == null) {
				exchange.getResponseSender().send(result);
				return;
			}

			List<UserScoreStatistic> users = new Vector<QueryFiveMysqlHandler.UserScoreStatistic>();
			users.add(userA);
			users.add(userB);

			result += String.format("%d\t%d\tWINNER\n", users.get(0).userId,
					users.get(1).userId);

			for (int idx = 1; idx <= 4; idx += 1) {
				long scoreA = users.get(0).getScore(idx), scoreB = users.get(1)
						.getScore(idx);
				result += scoreA + "\t" + scoreB + "\t";
				if (scoreA < scoreB) {
					result += users.get(1).userId;
				} else if (scoreA > scoreB) {
					result += users.get(0).userId;
				} else {
					result += "X";
				}
				result += '\n';
			}

			exchange.getResponseHeaders().put(Headers.CONTENT_TYPE,
					"text/plain");
			exchange.getResponseSender().send(result);
		}
	}
}
