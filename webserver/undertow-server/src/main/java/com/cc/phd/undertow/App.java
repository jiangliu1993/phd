package com.cc.phd.undertow;

import java.io.FileInputStream;
import java.util.Properties;

import com.cc.phd.undertow.utils.ClockThread;

public class App {

	public static void startNormalServer(Properties properties)
			throws Exception {
		String dbType = properties.getProperty("db.type");
		if (dbType.toLowerCase().equals("mysql")) {
			MysqlServer.startMysqlServer(properties);
		} else {
			HbaseServer.startHbaseServer(properties);
		}
	}

	public static void main(String[] args) {
		try {
            org.apache.log4j.BasicConfigurator.configure();
			ClockThread clock = new ClockThread();
			clock.start();
			Properties properties = new Properties();
			FileInputStream input = new FileInputStream("server.properties");
			properties.load(input);
			input.close();
			String serverRole = properties.getProperty("server.role");
			if (serverRole.equals("query3")) {
				CacheServer.startQueryThreeCacheServer(properties);
			} else if (serverRole.equals("query4")) {
				CacheServer.startQueryFourCacheServer(properties);
			} else if (serverRole.equals("offheap")) {
				CacheServer.startOffHeapCacheServer(properties);
			} else if (serverRole.equals("mix")) {
				MixServer.startMixServer(properties);
			} else if (serverRole.equals("proxy")) {
				ProxyServer.startProxyServer(properties);
			} else {
				startNormalServer(properties);
			}
		} catch (Exception e) {
			System.out.println("Server crashes...");
			e.printStackTrace();
		}
	}
}
