package com.cc.phd.undertow;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;
import org.xnio.Options;

import com.cc.phd.undertow.handler.QueryFourCacheHandler;
import com.cc.phd.undertow.handler.QueryOneHandler;
import com.cc.phd.undertow.handler.QueryThreeCacheHandler;

public class CacheServer {
	public static void startQueryThreeCacheServer(Properties properties)
			throws Exception {
		HashMap<String, String> friendsCache = new HashMap<String, String>();
		try (BufferedReader br = new BufferedReader(new FileReader(
				"retweet_post"))) {
			int count = 0;
			for (String line; (line = br.readLine()) != null;) {
				String[] temp = line.split("\t");
				friendsCache.put(temp[0], temp[1].replace("\\n", "\n"));
				count++;
				if (count % 100000 == 0) {
					System.out.println("Load: " + count);
				}
			}
			System.out.println("Cache load done");
		}
		Undertow.builder()
				.addHttpListener(
						Integer.parseInt(properties.getProperty("server.port")),
						"0.0.0.0")
				.setBufferSize(1024 * 16)
				.setIoThreads(Runtime.getRuntime().availableProcessors() * 2)
				.setSocketOption(Options.BACKLOG, 10000)
				.setServerOption(UndertowOptions.ALWAYS_SET_KEEP_ALIVE, false)
				.setServerOption(UndertowOptions.ALWAYS_SET_DATE, true)
				.setHandler(
						Handlers.path()
								.addExactPath("/q1", new QueryOneHandler())
								.addExactPath(
										"/q3",
										new QueryThreeCacheHandler(friendsCache)))
				.setWorkerThreads(200).build().start();
	}

	public static void startQueryFourCacheServer(Properties properties)
			throws Exception {
		HashMap<String, String[]> hashtagsCache = new HashMap<String, String[]>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream("hashtag_post"), "UTF8"))) {
			int count = 0;
			for (String line; (line = br.readLine()) != null;) {
				String[] temp = line.split("\t");
				int len = Integer.parseInt(temp[2]);
				String[] hashtags = new String[len];
				for (int i = 0; i < len; i++) {
					hashtags[i] = br.readLine().replace("\t", ":");
					count++;
					if (count % 100000 == 0) {
						System.out.println("Load: " + count);
					}
				}
				hashtagsCache.put(temp[0] + "\t" + temp[1], hashtags);
			}
			System.out.println("Cache load done");
		}
		Undertow.builder()
				.addHttpListener(
						Integer.parseInt(properties.getProperty("server.port")),
						"0.0.0.0")
				.setBufferSize(1024 * 16)
				.setIoThreads(Runtime.getRuntime().availableProcessors() * 2)
				.setSocketOption(Options.BACKLOG, 10000)
				.setServerOption(UndertowOptions.ALWAYS_SET_KEEP_ALIVE, false)
				.setServerOption(UndertowOptions.ALWAYS_SET_DATE, true)
				.setHandler(
						Handlers.path()
								.addExactPath("/q1", new QueryOneHandler())
								.addExactPath(
										"/q4",
										new QueryFourCacheHandler(hashtagsCache)))
				.setWorkerThreads(200).build().start();
	}

	public static void startOffHeapCacheServer(Properties properties)
			throws Exception {
		DB db = DBMaker.newMemoryDirectDB().transactionDisable().make();
		Map<String, String> friendsCache = db.createHashMap("friens")
				.keySerializer(Serializer.STRING)
				.valueSerializer(Serializer.STRING).make();
		try (BufferedReader br = new BufferedReader(new FileReader(
				"retweet_post"))) {
			int count = 0;
			for (String line; (line = br.readLine()) != null;) {
				String[] temp = line.split("\t");
				friendsCache.put(temp[0], temp[1].replace("\\n", "\n"));
				count++;
				if (count % 100000 == 0) {
					System.out.println("Load: " + count);
				}
			}
			System.out.println("Cache load done");
		}
		Map<String, String[]> hashtagsCache = db.createHashMap("hashtag")
				.keySerializer(Serializer.STRING).make();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream("hashtag_post"), "UTF8"))) {
			int count = 0;
			for (String line; (line = br.readLine()) != null;) {
				String[] temp = line.split("\t");
				int len = Integer.parseInt(temp[2]);
				String[] hashtags = new String[len];
				for (int i = 0; i < len; i++) {
					hashtags[i] = br.readLine().replace("\t", ":");
					count++;
					if (count % 100000 == 0) {
						System.out.println("Load: " + count);
					}
				}
				hashtagsCache.put(temp[0] + "\t" + temp[1], hashtags);
			}
			System.out.println("Cache load done");
		}
		Undertow.builder()
				.addHttpListener(
						Integer.parseInt(properties.getProperty("server.port")),
						"0.0.0.0")
				.setBufferSize(1024 * 16)
				.setIoThreads(Runtime.getRuntime().availableProcessors() * 2)
				.setSocketOption(Options.BACKLOG, 10000)
				.setServerOption(UndertowOptions.ALWAYS_SET_KEEP_ALIVE, false)
				.setServerOption(UndertowOptions.ALWAYS_SET_DATE, true)
				.setHandler(
						Handlers.path()
								.addExactPath("/q1", new QueryOneHandler())
								.addExactPath(
										"/q3",
										new QueryThreeCacheHandler(friendsCache))
								.addExactPath(
										"/q4",
										new QueryFourCacheHandler(hashtagsCache)))
				.setWorkerThreads(200).build().start();
	}
}
