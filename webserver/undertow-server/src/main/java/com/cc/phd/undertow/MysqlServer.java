package com.cc.phd.undertow;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.xnio.Options;

import com.cc.phd.undertow.handler.QueryFiveMysqlHandler;
import com.cc.phd.undertow.handler.QueryFourMysqlHandler;
import com.cc.phd.undertow.handler.QueryOneHandler;
import com.cc.phd.undertow.handler.QuerySixMysqlHandler;
import com.cc.phd.undertow.handler.QueryThreeDBHandler;
import com.cc.phd.undertow.handler.QueryTwoDBHandler;
import com.cc.phd.undertow.utils.Hashtag;
import com.cc.phd.undertow.utils.HashtagQuery;
import com.cc.phd.undertow.utils.Tweet;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class MysqlServer {
	static DataSource newDataSource(String uri, String user, String password) {
		GenericObjectPool connectionPool = new GenericObjectPool();
		connectionPool.setMaxActive(256);
		connectionPool.setMaxIdle(256);
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
				uri, user, password);
		new PoolableConnectionFactory(connectionFactory, connectionPool, null,
				null, false, true);
		return new PoolingDataSource(connectionPool);
	}
	
	public static void startMysqlServer(Properties properties) throws Exception {
		final DataSource mysql = newDataSource(
				properties.getProperty("mysql.uri"),
				properties.getProperty("mysql.user"),
				properties.getProperty("mysql.password"));
		final LoadingCache<String, LinkedList<Tweet>> tweetCache = CacheBuilder
				.newBuilder().build(
						new CacheLoader<String, LinkedList<Tweet>>() {
							@Override
							public LinkedList<Tweet> load(String query)
									throws Exception {
								String[] temp = query.split("\t");
								long userid = Long.parseLong(temp[0]);
								String tweet_time = temp[1];
								String sql = "SELECT tid, score, content FROM tweet"
										+ (userid % 4)
										+ " WHERE user_id = ? AND time = ? ORDER BY tid";
								try (Connection connection = mysql
										.getConnection();
										PreparedStatement statement = connection
												.prepareStatement(
														sql,
														ResultSet.TYPE_FORWARD_ONLY,
														ResultSet.CONCUR_READ_ONLY)) {
									statement.setLong(1, userid);
									statement.setString(2, tweet_time);
									try (ResultSet resultSet = statement
											.executeQuery()) {
										LinkedList<Tweet> results = new LinkedList<Tweet>();
										while (resultSet.next()) {
											results.add(new Tweet(resultSet
													.getLong(1), resultSet
													.getInt(2), resultSet
													.getString(3)));
										}
										return results;
									}
								}
							}
						});
		final LoadingCache<Long, String> friendsCache = CacheBuilder
				.newBuilder().build(new CacheLoader<Long, String>() {
					@Override
					public String load(Long userid) throws Exception {
						String sql = "SELECT retweeter_ids FROM retweet_post WHERE user_id = ?";
						try (Connection connection = mysql.getConnection();
								PreparedStatement statement = connection
										.prepareStatement(sql,
												ResultSet.TYPE_FORWARD_ONLY,
												ResultSet.CONCUR_READ_ONLY)) {
							statement.setLong(1, userid);
							try (ResultSet resultSet = statement.executeQuery()) {
								resultSet.next();
								return resultSet.getString(1);
							}
						}
					}
				});
		final LoadingCache<HashtagQuery, LinkedList<Hashtag>> hashtagCache = CacheBuilder
				.newBuilder().build(
						new CacheLoader<HashtagQuery, LinkedList<Hashtag>>() {
							@Override
							public LinkedList<Hashtag> load(HashtagQuery query)
									throws Exception {
								String sql = "SELECT hashtag, tweet_ids FROM hashtag WHERE day = ? AND location = ? ORDER BY popularity DESC, first_tweet_id ASC, indice ASC LIMIT ?,?";
								try (Connection connection = mysql
										.getConnection();
										PreparedStatement statement = connection
												.prepareStatement(
														sql,
														ResultSet.TYPE_FORWARD_ONLY,
														ResultSet.CONCUR_READ_ONLY)) {
									statement.setString(1, query.date);
									statement.setString(2, query.location);
									statement.setInt(3, query.m - 1);
									statement.setInt(4, query.n - query.m + 1);
									try (ResultSet resultSet = statement
											.executeQuery()) {
										LinkedList<Hashtag> results = new LinkedList<Hashtag>();
										int rank = query.m;
										while (resultSet.next()) {
											results.add(new Hashtag(resultSet
													.getString(1), resultSet
													.getString(2), rank));
											rank++;
										}
										return results;
									}
								}
							}
						});
		Undertow.builder()
				.addHttpListener(
						Integer.parseInt(properties.getProperty("server.port")),
						"0.0.0.0")
				.setBufferSize(1024 * 16)
				.setIoThreads(Runtime.getRuntime().availableProcessors() * 2)
				.setSocketOption(Options.BACKLOG, 10000)
				.setServerOption(UndertowOptions.ALWAYS_SET_KEEP_ALIVE, false)
				.setServerOption(UndertowOptions.ALWAYS_SET_DATE, true)
				.setHandler(
							Handlers.path()
								.addExactPath("/q1", new QueryOneHandler())
								.addExactPath("/q2", new QueryTwoDBHandler(tweetCache))
								.addExactPath("/q3", new QueryThreeDBHandler(friendsCache))
								.addExactPath("/q4", new QueryFourMysqlHandler(hashtagCache, mysql))
								.addExactPath("/q5", new QueryFiveMysqlHandler(mysql))
								.addExactPath("/q6", new QuerySixMysqlHandler(mysql))
							).setWorkerThreads(200)
								
				.build().start();
	}
}
