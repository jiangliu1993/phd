package com.cc.phd.undertow.handler;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

import java.math.BigInteger;
import java.util.Deque;
import java.util.Map;

public class QueryOneHandler implements HttpHandler {

	static final BigInteger x = new BigInteger(
			"6876766832351765396496377534476050002970857483815262918450355869850085167053394672634315391224052153");
	static final String teamInfo = "PermanentHeadDamage,1563-6721-1410,1421-8919-2387,9620-2216-5685\n";

	public static String time = "Waiting for update\n\n";
	
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		Map<String, Deque<String>> query = exchange.getQueryParameters();
		Deque<String> values = query.get("key");
		if (values == null) {
			return;
		}
		String value = values.peek();
		if (value == null) {
			return;
		}
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		try {
			BigInteger xy = new BigInteger(value);
			BigInteger y = xy.divide(x);
			String result = y.toString() + "\n";
			result += teamInfo;
			result += time;
			exchange.getResponseSender().send(result);
		} catch (NumberFormatException e) {
		}
	}
}
