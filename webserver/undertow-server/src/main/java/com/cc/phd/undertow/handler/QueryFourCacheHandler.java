package com.cc.phd.undertow.handler;

import java.util.Deque;
import java.util.Map;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class QueryFourCacheHandler implements HttpHandler {

	static final String teamInfo = "PermanentHeadDamage,1563-6721-1410,1421-8919-2387,9620-2216-5685\n";
	Map<String, String[]> hashtagsCache;

	public QueryFourCacheHandler(Map<String, String[]> hashtagsCache) {
		super();
		this.hashtagsCache = hashtagsCache;
	}

	public void handleRequest(HttpServerExchange exchange) throws Exception {
		String result = teamInfo;
		Map<String, Deque<String>> query = exchange.getQueryParameters();
		Deque<String> date_values = query.get("date");
		if (date_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String date_value = date_values.peek();
		if (date_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		Deque<String> location_values = query.get("location");
		if (location_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String location_value = location_values.peek();
		if (location_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		Deque<String> m_values = query.get("m");
		if (m_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String m_value = m_values.peek();
		if (m_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		Deque<String> n_values = query.get("n");
		if (n_values == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		String n_value = n_values.peek();
		if (n_value == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		/* Should not block IO threads */
		if (exchange.isInIoThread()) {
			exchange.dispatch(this);
			return;
		}
		String[] hashtags = hashtagsCache.get(location_value + "\t"
				+ date_value);
		if (hashtags == null) {
			exchange.getResponseSender().send(result);
			return;
		}
		try {
			for (int i = Integer.parseInt(m_value) - 1; i < Integer.parseInt(n_value); i++) {
				result += hashtags[i] + "\n";
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			
		}
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
		exchange.getResponseSender().send(result);
	}
}
